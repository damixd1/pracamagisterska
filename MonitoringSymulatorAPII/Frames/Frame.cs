﻿namespace Frames
{
    public class Frame
    {
        public int lengthOfHeader { get; set; }
        public int payLoadLength { get; set; }
        public int totalLengthOfFrame { get; set; }

        public virtual int TotalLengthOfFrame(int payLoadLength)
        {
            totalLengthOfFrame = payLoadLength + lengthOfHeader;
            return totalLengthOfFrame;
        }
    }
}