﻿using Symulator;

namespace Frames
{
    public class ExtensionObject
    {
        public ExtensionObject(string address, string timeStamp, int fragmentId, string data)
        //wlasny dodatkowy nagłowek
        {
            this.Address = address;
            this.TimeStamp = timeStamp;
            this.FragmentId = fragmentId;
            this.Data = data;
        }

        public string Address { get; set; }
        public string TimeStamp { get; set; }
        public int FragmentId { get; set; }
        public string Data { get; set; }

        public static int LengthOfObjectM1 { get; set; } = 16 + 4 + 4 + Router.maxAddedBytesPerNode;
        public static int LengthOfObjectM2 { get; set; } = 4 + 4 + Router.maxAddedBytesPerNode;
    }
}