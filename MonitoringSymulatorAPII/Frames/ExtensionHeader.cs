﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Symulator;
using TBRP;

namespace Frames
{
    public class ExtensionHeader : Frame
    {
        public ExtensionHeader()
        {
            listOfData = new List<ExtensionObject>();
        }

        public int quantityOfSegments { get; set; } = 0; //ilość segmentów
        public List<ExtensionObject> listOfData { get; set; }

        public override int TotalLengthOfFrame(int payLoadLength)
        {
            if (payLoadLength == 1)
            {
                totalLengthOfFrame = listOfData.Count * ExtensionObject.LengthOfObjectM1 + 4;
                //4 oznacza 4 bajty niezbedne (Next header, count, reserved)
            }
            else
            {
                totalLengthOfFrame = listOfData.Count * ExtensionObject.LengthOfObjectM2;
            }

            return totalLengthOfFrame;
        }


        public static List<List<string>> DivideTree(ConcurrentDictionary<string, TBRP_TableRecord> TBRP_RoutingTable, int quantity)
        {
            var abc = new Dictionary<string, TBRP_TableRecord>(TBRP_RoutingTable);

            var Count = abc.Count;
            var Packet_quantity = (int)Math.Ceiling((double)Count / quantity); //ilosc potrzebnych pakietow

            //jakie adresy nalezy wpisac do poszczegolnych pakietow

            var AddressesInPackets = new List<List<string>>();

            var counter = 1;

            for (var i = 0; i < Packet_quantity; i++)
            {
                var temp = new List<string>();
                for (var j = 0; j < quantity; j++)
                {
                    if (counter > Count)
                    {
                        break;
                    }

                    var temp_min = abc.Values.Aggregate((v1, v2) => v1.HopCount < v2.HopCount ? v1 : v2);
                    temp.Add(temp_min.DestinationIPv6);
                    abc.Remove(temp_min.DestinationIPv6);
                    counter++;
                }
                AddressesInPackets.Add(temp);

                if (counter > Count)
                {
                    break;
                }
            }

            return AddressesInPackets;
        }
    }
}