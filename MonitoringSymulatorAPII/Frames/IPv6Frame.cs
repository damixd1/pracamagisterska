﻿using System;
using Symulator;

namespace Frames
{
    public class IPv6Frame : Frame
    {
        public IPv6Frame()
        {
            lengthOfHeader = 40;
        }

        //public string PayLoad { get; set; }
        public string SourceAddress { get; set; } = null;
        public string DestinationAddress { get; set; } = null;
        /*public int NextHeader { get; set; }
        public int HopLimit { get; set; }
        public IPv6_RoutingHeaderFormat RoutingHeader { get; set; }
        public ExtensionHeader Extension { get; set; }*/

    }
}