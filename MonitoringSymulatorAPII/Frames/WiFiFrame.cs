﻿namespace Frames
{
    public class WiFiFrame : Frame
    {
        public int maxLengthOfPayLoad { get; set; } = 2304;

        public WiFiFrame()
        {
            lengthOfHeader = 34;
        }

        public string DestMac { get; set; } = null;
        public string SourceMac { get; set; } = null;
        //public string PayLoad { get; set; } = null;
    }
}