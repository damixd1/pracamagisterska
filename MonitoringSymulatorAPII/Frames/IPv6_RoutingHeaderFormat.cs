﻿using System;
using System.Collections.Generic;

namespace Frames
{
    public class IPv6_RoutingHeaderFormat : Frame
    {
        public IPv6_RoutingHeaderFormat(List<string> Addresses, int SegmentsLeft)
        {
            this.Addresses = Addresses;
            this.SegmentsLeft = SegmentsLeft;
            HdrExtLen = 2 * Addresses.Count;
            lengthOfHeader = Addresses.Count * 16 + 4 + 4;
        }

        public int HdrExtLen { get; set; }
        public int SegmentsLeft { get; set; }
        public List<string> Addresses { get; set; }
    }
}