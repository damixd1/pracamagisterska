﻿using System;
using System.Collections.Generic;

namespace Symulator
{
    public class UserPacketIPGenerator
    {
        private readonly List<string> Addresses;
        private List<string> onlyThisAddresses;

        public UserPacketIPGenerator(string myIPAddress, List<string> onlyThisAddresses)
        {
            Addresses = new List<string>();
            this.onlyThisAddresses = onlyThisAddresses;

            foreach (var item in onlyThisAddresses)
            {
                if (!string.Equals(item, myIPAddress))
                {
                    Addresses.Add(item);
                }
            }
        }

        public string RandomDestination()
        {
            var randomize = new Random();
            return Addresses[randomize.Next(0, Addresses.Count)];
        }
    }
}