﻿using System;
using Frames;

namespace TBRP
{
    public class TBRP_RREQ : Frame
    {
        public TBRP_RREQ()
        {
            lengthOfHeader = 59;
        }

        public int TBRP_RREQ_ID { get; set; }
        public int Metric { get; set; }
        public int HopCount { get; set; }
        public string DestinationAddress { get; set; }
        public string OriginatorAddress { get; set; }

        public int DestinationSequenceNumber { get; set; }
        public int OriginatorSequenceNumber { get; set; }
        /*public int ElementID { get; set; }
        public int Length { get; set; }
        public int TTL { get; set; }
        public int Flags { get; set; }*/

    }
}