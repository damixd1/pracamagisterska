﻿using Symulator;

namespace TBRP
{
    public class TBRP_TableRecord
    {
        public string DestinationIPv6 { get; set; }
        public int Metric { get; set; }
        public int HopCount { get; set; }
        public Router Interface { get; set; }
        public int DestinationSequenceNumber { get; set; }
    }
}