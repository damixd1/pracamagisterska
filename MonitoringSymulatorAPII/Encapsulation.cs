﻿using AODV;
using Frames;
using TBRP;

namespace Symulator
{
    public class Encapsulation
    {
        public IPv6Frame IP_F { get; set; }
        public UDPFrame UDP_F { get; set; }
        public AODV_RREQ AODV_RREQ_F { get; set; }
        public AODV_RREP AODV_RREP_F { get; set; }
        public TBRP_RREQ TBRP_PREQ_F { get; set; }
        public TBRP_RREP TBRP_PREP_F { get; set; }
        public TCPFrame TCP_F { get; set; }
        public IPv6_RoutingHeaderFormat IP_Routing { get; set; }
        public ExtensionHeader Extension { get; set; }
        public WiFiFrame WiFi_F { get; set; }
        public int totalLengthOfFrame { get; private set; }
        public int ID { get; set; }

        public int goThroughLinkCount { get; set; } = 0;
        public int goThroughLinkDataSum { get; set; } = 0;
        public int goThroughAodvCount { get; set; } = 0;


        public void Make(int id, WiFiFrame WiFi_F, IPv6Frame IP_F, UDPFrame UDP_F, AODV_RREQ AODV_RREQ_F)
        {
            this.WiFi_F = WiFi_F;
            this.IP_F = IP_F;
            this.UDP_F = UDP_F;
            this.AODV_RREQ_F = AODV_RREQ_F;

            ID = id;

            totalLengthOfFrame =
                WiFi_F.TotalLengthOfFrame(IP_F.TotalLengthOfFrame(UDP_F.TotalLengthOfFrame(AODV_RREQ_F.lengthOfHeader)));
        }

        public void Make(int id, WiFiFrame WiFi_F, IPv6Frame IP_F, UDPFrame UDP_F, AODV_RREP AODV_RREP_F)
        {
            this.WiFi_F = WiFi_F;
            this.IP_F = IP_F;
            this.UDP_F = UDP_F;
            this.AODV_RREP_F = AODV_RREP_F;

            ID = id;

            totalLengthOfFrame =
                WiFi_F.TotalLengthOfFrame(IP_F.TotalLengthOfFrame(UDP_F.TotalLengthOfFrame(AODV_RREP_F.lengthOfHeader)));
        }

        public void Make(int id, WiFiFrame WiFi_F, IPv6Frame IP_F, TCPFrame TCP_F)
        {
            this.WiFi_F = WiFi_F;
            this.IP_F = IP_F;
            this.TCP_F = TCP_F;

            ID = id;

            totalLengthOfFrame =
                WiFi_F.TotalLengthOfFrame(IP_F.TotalLengthOfFrame(TCP_F.TotalLengthOfFrame(TCP_F.payLoadLength)));
        }

        public void Make(int id, WiFiFrame WiFi_F, IPv6Frame IP_F, UDPFrame UDP_F, TBRP_RREQ TBRP_PREQ_F)
        {
            this.WiFi_F = WiFi_F;
            this.IP_F = IP_F;
            this.UDP_F = UDP_F;
            this.TBRP_PREQ_F = TBRP_PREQ_F;

            ID = id;

            totalLengthOfFrame =
                WiFi_F.TotalLengthOfFrame(IP_F.TotalLengthOfFrame(UDP_F.TotalLengthOfFrame(TBRP_PREQ_F.lengthOfHeader)));
        }

        public void Make(int id, WiFiFrame WiFi_F, IPv6Frame IP_F, UDPFrame UDP_F, TBRP_RREP TBRP_PREP_F)
        {
            this.WiFi_F = WiFi_F;
            this.IP_F = IP_F;
            this.UDP_F = UDP_F;
            this.TBRP_PREP_F = TBRP_PREP_F;

            ID = id;

            totalLengthOfFrame =
                WiFi_F.TotalLengthOfFrame(IP_F.TotalLengthOfFrame(UDP_F.TotalLengthOfFrame(TBRP_PREP_F.lengthOfHeader)));
        }

        public void Make(int id, WiFiFrame WiFi_F, IPv6Frame IP_F, IPv6_RoutingHeaderFormat IP_Routing, TCPFrame TCP_F, ExtensionHeader Extension)
        {
            this.WiFi_F = WiFi_F;
            this.IP_F = IP_F;
            this.TCP_F = TCP_F;
            this.IP_Routing = IP_Routing;

            ID = id;

            totalLengthOfFrame =
                WiFi_F.TotalLengthOfFrame(IP_Routing.lengthOfHeader +
                                          IP_F.TotalLengthOfFrame(
                                              TCP_F.TotalLengthOfFrame(Extension.TotalLengthOfFrame(2))));
        }

        public void Make(int id, WiFiFrame WiFi_F, IPv6Frame IP_F, ExtensionHeader Extension, TCPFrame TCP_F)
        {
            this.WiFi_F = WiFi_F;
            this.IP_F = IP_F;
            this.TCP_F = TCP_F;
            this.Extension = Extension;

            ID = id;

            totalLengthOfFrame =
                WiFi_F.TotalLengthOfFrame(Extension.TotalLengthOfFrame(0) +
                                          IP_F.TotalLengthOfFrame(TCP_F.TotalLengthOfFrame(TCP_F.payLoadLength)));
        }
    }
}