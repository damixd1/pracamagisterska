﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Symulator
{
    public sealed class Stats
    {
        private static Stats instance;
        private ConcurrentDictionary<string, int> AODVData;
        private ConcurrentDictionary<string, int> monitoringData;
        public ConcurrentDictionary<string, int> TBRPData;
        public ConcurrentDictionary<int, int> TBRPData_Nodes;
        public int totalCount = 0;
        public int linkCount { get; set; }
        public int nodesCount { get; set; }
        public bool tbrpfinished = false;
        private ConcurrentDictionary<int, Tuple<int, int>> extendedStats;
        private ConcurrentDictionary<int, int> packetAODVCount;
        public ConcurrentDictionary<int, int> TBRPpacketDataSum;
        public List<List<int>> raportList { get; set; }
        public Dictionary<string,int> refIPtoID { get; set; }
        public Dictionary<string, int> showIDlist { get; set; }
        public bool adInfoAboutMonioredNodes { get; set; } = false;
        private bool goExit = false;
        public Router root;

        private Stats()
        {
            monitoringData = new ConcurrentDictionary<string, int>();
            nodesToMonitor = new ConcurrentDictionary<string, bool>();
            nodesToMonitorTime = new ConcurrentDictionary<string, DateTime>();
            AODVData = new ConcurrentDictionary<string, int>();
            TBRPData = new ConcurrentDictionary<string, int>();
            TBRPData_Nodes = new ConcurrentDictionary<int, int>();
            extendedStats = new ConcurrentDictionary<int, Tuple<int, int>>();
            packetAODVCount = new ConcurrentDictionary<int, int>();
            raportList = new List<List<int>>();
            refIPtoID = new Dictionary<string, int>();
            showIDlist = new Dictionary<string, int>();
            TBRPpacketDataSum = new ConcurrentDictionary<int, int>();
        }

        public ConcurrentDictionary<string, bool> nodesToMonitor { get; set; }
        public ConcurrentDictionary<string, DateTime> nodesToMonitorTime { get; set; }
        public DateTime startMonitoringTime { get; set; }
        public DateTime startRoutingTime { get; set; }
        public int TBRPtableCount { get; set; }
        public int AODVcount { get; set; } = 0;

        public Program instance2 { get; set; }

        private int MaxLinkCount = 0;
        private int MaxLinkData = 0;
        private int adovPacketcount = 0;
        private int linkaccident = 0;
        public float timeMS;

        public static Stats Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Stats();
                }

                return instance;
            }
        }

        public void RaportExtendedvF(int linkCount, int linkDataSum, int packetAodvCount)
        {
            int count = extendedStats.Count;
            extendedStats.AddOrUpdate(count, new Tuple<int, int>(linkCount, linkDataSum), (key, oldValue) => new Tuple<int, int>(linkCount, linkDataSum));

            packetAODVCount.AddOrUpdate(count, packetAodvCount, (key, oldValue) => oldValue);
        }

        public void RaportExtendedvU(int linkCount, int linkDataSum, int packetAodvCount)
        {
            if (linkDataSum > MaxLinkData)
            {
                MaxLinkData = linkDataSum;
                MaxLinkCount = linkCount;
            }

            if (packetAodvCount > adovPacketcount)
            {
                adovPacketcount = packetAodvCount;
            }

                
        }

        public void RaportPacketvFRoute(int nodeID, int packetID)
        {
            if (raportList.Count < packetID)
            {
                raportList.Add(new List<int>());
            }

            raportList[packetID - 1].Add(nodeID);
        }

        public void Raport(string data1, int data2)
        {
            monitoringData.AddOrUpdate(data1, data2, (key, oldValue) => oldValue + data2);
        }

        public void RaportAODV(string data1, int data2)
        {
            AODVData.AddOrUpdate(data1, data2, (key, oldValue) => oldValue + data2);
        }

        public void RaportTBRP(string data1, int data2)
        {
            TBRPData.AddOrUpdate(data1, data2, (key, oldValue) => oldValue + data2);
        }

        private int NodesLeft()
        {
            return nodesToMonitor.Values.Count(v => v);
        }

        public void ShowDiagnostic(string txt)
        {
            Trace.WriteLine("OK. " + txt);
            instance2.Dispatcher.BeginInvoke(new Action(() => { instance2.textBlock_Stats.Text += txt + "\n"; }));
            instance2.Dispatcher.BeginInvoke(new Action(() => { instance2.textBlock_Stats.Focus(); }));
            instance2.Dispatcher.BeginInvoke(new Action(() => { instance2.textBlock_Stats.CaretIndex = instance2.textBlock_Stats.Text.Length - 1; }));

        }

        public void ShowGraphStats(int nodes, int links)
        {
            nodesCount = nodes;
            instance2.Dispatcher.BeginInvoke(new Action(() => { instance2.label_ShowNodesCount.Content = "Liczba węzłów: " + nodes + ""; }));
            instance2.Dispatcher.BeginInvoke(new Action(() => { instance2.label_ShowLinksCount.Content = "Liczba łączy: " + links + ""; }));
        }

        public void ShowTBRP(int tbrp)
        {
            instance2.Dispatcher.BeginInvoke(new Action(() => { instance2.label_ShowRootTabSize.Content = "Rozmiar tablicy korzenia TBRP: " + tbrp + ""; }));

            if (tbrp == 0)
            {
                goExit = true;
            }
        }

        public void Showinterval(int inter)
        {
            instance2.Dispatcher.BeginInvoke(new Action(() => { instance2.label_ShowStatsInterval.Content = "Interwał statystyk: "+ (float)inter/1000 + "[s]"; }));
        }

        public void LinkAccidentReport()
        {
            linkaccident++;
        }

        private double calculateMS(int nodesCount, int dataCount)
        {
            return Math.Round((double)(nodesCount + (double)dataCount / (10*1024*1024/8)*1000), 2);
        }

        public bool ShowStats(MonitoringType type)
        {
            if (TBRPtableCount != 0)
            {
                var nodesleft = NodesLeft();
                float test = 100 * nodesleft / TBRPtableCount;
                
                ShowDiagnostic("Odebrano dane z " + test + "% węzłów (" + nodesleft + " / " + TBRPtableCount + ")");
                int counter = 0;
                string listtodisplay = null;

                if (adInfoAboutMonioredNodes == true)
                {
                    foreach (var item in nodesToMonitor)
                    {
                        if (item.Value == true)
                        {
                            if (!showIDlist.ContainsKey(item.Key))
                            {
                                showIDlist.Add(item.Key, 0);
                                counter++;
                                listtodisplay += refIPtoID[item.Key] + ", ";
                            }
                        }                      
                    }

                    if (counter != 0)
                    {
                        listtodisplay = listtodisplay.Remove(listtodisplay.Length - 2);
                        ShowDiagnostic("[" + listtodisplay + "]");
                    }
                    
                    listtodisplay = null;
                }

                ulong sum = 0;
                foreach (var item in monitoringData)
                {
                    sum += (ulong)item.Value;
                }

                if (test == 100)
                {
                    Router.isMonitored = true;

                    if (linkaccident != 0)
                    {
                        ShowDiagnostic(" ");
                        ShowDiagnostic("Odnotowwano awarię " + linkaccident + " łaczy");
                    }

                    ShowDiagnostic(" ");

                    if (type == MonitoringType.SNMP)
                    {
                        ShowDiagnostic("Monitoring SNMP wygenerował dodatkowo " + sum + " bajtów");
                    }
                    else
                    {
                        ShowDiagnostic("Monitoring EHBMP version " + type + " wygenerował dodatkowo " + sum + " bajtów");
                    }

                    ShowDiagnostic("Minimalnie dodatkowe obciążenie na jednym łączu: " + monitoringData.Values.Min() + " bajtów");
                    ShowDiagnostic("Maksymalnie dodatkowe obciążenie na jednym łączu: " + monitoringData.Values.Max() + " bajtów");                  
                    

                  /* if (MonitoringType.Unforced == type)
                    {
                        ShowDiagnostic("Odebrano " + TBRPtableCount + " paczek danych");
                     
                    }
                    else
                    {
                        ShowDiagnostic("Odebrano " + totalCount + " paczek danych");                   
                    }*/

                    if (AODVcount > 0)
                    {
                        ShowDiagnostic(" ");
                        if (linkaccident != 0)
                        {
                            ShowDiagnostic("Ilość uruchomionych RM-AODV na potrzeby monitoringu: " + AODVcount + " w tym " + linkaccident + " w wyniku awarii łącza");
                        }
                        else
                        {
                            ShowDiagnostic("Ilość uruchomionych RM-AODV na potrzeby monitoringu: " + AODVcount);
                        }
              
                        ulong sum2 = 0;
                        foreach (var item in AODVData)
                        {
                            sum2 += (ulong)item.Value;
                        }
                        ShowDiagnostic(" ");
                        if (sum2 != 0)
                        {
                            ShowDiagnostic("Na potrzeby RM-AODV wygenerowano dodatkowo " + sum2 + " bajtów (średnio " + sum2 / (ulong)AODVcount + ")");
                            ShowDiagnostic("Minimalnie dodatkowe obciążenie na jednym łączu: " + AODVData.Values.Min() + " bajtów (średnio " + (float)(AODVData.Values.Min() / AODVcount) + ")");
                            ShowDiagnostic("Maksymalnie dodatkowe obciążenie na jednym łączu: " + AODVData.Values.Max() + " bajtów (średnio " + (float)(AODVData.Values.Max() / AODVcount) + ")");
                        }                 
                    }

                    ulong sum3 = 0;
                    foreach (var item in TBRPData)
                    {
                        sum3 += (ulong)item.Value;
                    }


                    ShowDiagnostic(" ");
                    ShowDiagnostic("Rozmiar tablicy routingu korzenia drzewa TBRP: " + TBRPtableCount);
                    ShowDiagnostic("Na potrzeby jednego TBRP wygenerowano dodatkowo " + sum3 + " bajtów");
                    ShowDiagnostic("Minimalnie dodatkowe obciążenie na jednym łączu: " + TBRPData.Values.Min() +
                                    " bajtów");
                    ShowDiagnostic("Maksymalnie dodatkowe obciążenie na jednym łączu: " + TBRPData.Values.Max() +
                                    " bajtów");

                    var timepsan = startMonitoringTime.Subtract(startRoutingTime).TotalSeconds;
                    ShowDiagnostic(" ");
                    ShowDiagnostic("Czas symulacji routingu TBRP: " + Math.Round(timepsan, 2) + " sekund");

                    var T2 = Task.Run(() => root.SendSNMP());
                    Task.WaitAll(T2);

                    Thread.Sleep(100);

                    var dataTBRP = TBRPpacketDataSum.Values.Max();

                    var nodesTBRP = TBRPData_Nodes.Values.Sum()/TBRPData_Nodes.Count;

                    ShowDiagnostic("Możliwy przybliżony rzeczywisty czas wykonywania routingu TBRP to " + calculateMS(nodesTBRP, dataTBRP) + " ms na transmisję danych");

                    var timepsan2 = nodesToMonitorTime.Values.Max().Subtract(startMonitoringTime).TotalSeconds;
                    ShowDiagnostic(" ");
                    var time = Math.Round(timepsan2, 2) + 1;
                    ShowDiagnostic("Czas symulacji monitoringu: " + time + " sekund");
                    

                    if (MonitoringType.Unforced == type)
                    {
                        ShowDiagnostic("Możliwy przybliżony rzeczywisty czas wykonywania monitoringu: " + timeMS + " sekund oczekiwania na własciwy pakiet" +
                                       " zakładając że co " + (float)Router.timeIntervalUms/1000 + " sekund generowany jest pakiet o rozmiarze pola danych wylosowanym z generatora o rozkładzie równomiernym, w kierunku centrum monitoringu");
                        ShowDiagnostic("I dodatkowo " + calculateMS(MaxLinkCount, MaxLinkData) + " ms na transmisję danych monitorujących przez łącza");

                        if (linkaccident > 0)
                        {
                            ShowDiagnostic("I dodatkowo około " + (linkaccident * 10) +
                                       " ms na potrzeby monitoringu RM-AODV, zakładając, że średnio na jedno znalezienie trasy na żądanie zajmuje około 10 ms (im bliżej jest szukany węzeł tym trasa jest znajdywana szybciej).");
                        }
                    }
                    else if (MonitoringType.Forced == type)
                    {

                        int maxLinkCount = 0;
                        int maxLinkData = 0;
                        int maxlinkCountDictKey = 0;
                        int maxlinkDataDictKey = 0;

                        foreach (var item in extendedStats)
                        {
                            if (item.Value.Item1 > maxLinkCount)
                            {
                                maxLinkCount = item.Value.Item1;
                                maxlinkCountDictKey = item.Key;
                            }

                            if (item.Value.Item2 > maxLinkData)
                            {
                                maxLinkData = item.Value.Item2;
                                maxlinkDataDictKey = item.Key;
                            }
                        }

                        int maxAODVCount = 0;
                        int maxAODVCountDictKey = 0;

                        foreach (var item in packetAODVCount)
                        {
                            if (item.Value > maxAODVCount)
                            {
                                maxAODVCount = item.Value;
                                maxAODVCountDictKey = item.Key;
                            }
                        }
                        
                        ShowDiagnostic("Możliwy przybliżony rzeczywisty czas wykonywania monitoringu to " + calculateMS(extendedStats[maxlinkDataDictKey].Item1, maxLinkData) +
                                       " ms na transmisję danych monitorujących przez łącza");
                        ShowDiagnostic("I dodatkowo około " + (maxAODVCount*10) +
                                       " ms na potrzeby monitoringu RM-AODV, zakładając, że średnio na jedno znalezienie trasy na żądanie zajmuje około 10 ms (im bliżej jest szukany węzeł tym trasa jest znajdywana szybciej).");                      
                    }
                    else
                    {
                        ShowDiagnostic("Możliwy przybliżony rzeczywisty czas wykonywania monitoringu to " + calculateMS(MaxLinkCount*2, MaxLinkData) + " ms na transmisję danych monitorujących przez łącza");

                        if (linkaccident > 0)
                        {
                            ShowDiagnostic("I dodatkowo około " + (linkaccident * 10) +
                                       " ms na potrzeby monitoringu RM-AODV, zakładając, że średnio na jedno znalezienie trasy na żądanie zajmuje około 10 ms (im bliżej jest szukany węzeł tym trasa jest znajdywana szybciej).");
                        }
                    }

                    ShowDiagnostic("Czas potrzebny na transmisję danych oszacacowano, zakładajac że wszystkie łacza mają przepustowość 10Mb/s, a opóźnienie przetwarzania w węźle to 1 ms.");

                    instance2.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        instance2.button_StartSymulation.Content = "Aby przeprowadzić kolejny eksperyment symulacyjny\nzamknij aplikację symulatora i uruchom ją ponownie";
                    }));
                    return true;
                }
            }
            else
            {
                if (goExit == false)
                {
                    ShowDiagnostic("Oczekiwanie na start monitoringu...");
                }
                else
                {
                    ShowDiagnostic(" ");
                    ShowDiagnostic("Wybrany węzeł ROOT ma pustą tablice routingu, symulacja monitoringu nie może być kontynułowana!");
                    return true;
                }
               
            }

            return false;
        }
    }
}