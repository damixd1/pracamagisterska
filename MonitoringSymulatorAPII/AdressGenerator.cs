﻿using System;

namespace Symulator
{
    public static class AdressGenerator
    {
        public static string GenerateNewMac(Random randomize)
        {
            string mac = null;

            for (var i = 0; i < 48; i++)
            {
                mac += Convert.ToString(randomize.Next(0, 2));
            }

            return mac;
        }

        public static string GenerateNewIPv6(Random randomize)
        {
            string ipv6 = null;

            for (var i = 0; i < 128; i++)
            {
                ipv6 += Convert.ToString(randomize.Next(0, 2));
            }

            return ipv6;
        }
    }
}