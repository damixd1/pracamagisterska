﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using AODV;
using Frames;
using Timer = System.Timers.Timer;
using TBRP;
using System.Diagnostics;

namespace Symulator
{
    public class Router
    {
        public static bool SNMP = false;
        public int time_ms = 0;
        public static bool isaccidentposible = false;
        public static int timeIntervalUms { get; set; } = 1000;

        //Monitoring Method 1
        public static int maxNodeQuantityToVisit { get; set; } = 14;
        public static int maxAddedBytesPerNode { get; set; }
        public static bool isMonitored { get; set; } = false;
        public static int payLoadWifi { get; set; } = 2304;
        private int nodesCount;
        private static int packetID = 0;
        public MonitoringType choise;

        //AODV
        private int AODV_RouterOwnSequenceNumber;
        private ConcurrentDictionary<string, AODV_TableRecord> AODV_RoutingTable;
        private int AODV_RREQ_ID;
        private ConcurrentDictionary<Tuple<int, int>, DateTime> DiscardAODV_RREQ;
        private ConcurrentDictionary<Tuple<int, int>, DateTime> DiscardTBRP_PREQ;
        private bool isTBRPconvergenced;

        private readonly int MY_ROUTE_TIMEOUT = 200000;
        //in milisecond -> 2 minuty czas na aktywnosc trasy w tabeli routingu AODV
        private readonly int PATH_DISCOVERY_TIME = 400000;
        private bool needSendData = true;

        //Others
        private UserPacketIPGenerator newDestination; //losuje do kogo ma byc wyslana wiadomość

        public bool stopThread;
        private int TBRP_RouterOwnSequenceNumber;
        public ConcurrentDictionary<string, TBRP_TableRecord> TBRP_RoutingTable;

        //TBRP
        private int TBRP_PREQ_ID;
        private PerformanceCounter cpuCounter;

        //Router's main information
        public int RouterID { get; set; }
        public List<Router> nodes { get; set; }
        public string MAC { get; set; }
        public string IPv6 { get; set; }
        public ConcurrentDictionary<Router, int> Interfaces { get; set; } //Value -> Metrics


        public Router()
        {
            Interfaces = new ConcurrentDictionary<Router, int>();
            DiscardAODV_RREQ = new ConcurrentDictionary<Tuple<int, int>, DateTime>();

            AODV_RoutingTable = new ConcurrentDictionary<string, AODV_TableRecord>();
            AODV_RREQ_ID = 0;
            AODV_RouterOwnSequenceNumber = 1;

            TBRP_RoutingTable = new ConcurrentDictionary<string, TBRP_TableRecord>();
            TBRP_PREQ_ID = 0;
            TBRP_RouterOwnSequenceNumber = 1;
            DiscardTBRP_PREQ = new ConcurrentDictionary<Tuple<int, int>, DateTime>();

            cpuCounter = new PerformanceCounter();
            cpuCounter.CategoryName = "Processor";
            cpuCounter.CounterName = "% Processor Time";
            cpuCounter.InstanceName = "_TOTAL";
        }

        public void Start(List<Router> AllNodes, MonitoringType choise)
        {
            nodes = AllNodes;
            nodesCount = nodes.Count;
            this.choise = choise;

            switch (choise)
            {
                case MonitoringType.Forced:
                    if (RouterID == 1)
                    {
                        Stats.Instance.root = this;
                        var T = Task.Run(() => TBRP_PREQ_RoutingProcess());
                        var T2 = Task.Run(() => SendEHBMPvF());
                    }
                    break;
                case MonitoringType.Unforced:
                    // SetRouterMonitoringTimer();

                    if (RouterID == 1)
                    {
                        Stats.Instance.root = this;
                        var T = Task.Run(() => TBRP_PREQ_RoutingProcess());
                        //Task.WaitAll(T);
                        var T2 = Task.Run(() => userAplicationSimulatedProcess());
                    }
                    break;
                case MonitoringType.SNMP:
                    if (RouterID == 1)
                    {
                        Stats.Instance.root = this;
                        var T = Task.Run(() => TBRP_PREQ_RoutingProcess());
                        var T2 = Task.Run(() => SendSNMP());
                    }
                    break;
                default:

                    break;
            }
        }


        public void SendSNMP()
        {
            Router.SNMP = true;
            Thread.Sleep(1000);

            while (true)
            {
                if (isTBRPconvergenced)
                {
                    foreach (var item in nodes.Skip(1))
                    {
                        var Packet = new Encapsulation();
                        var ipv6 = new IPv6Frame();
                        ipv6.DestinationAddress = item.IPv6;
                        var tcp = new TCPFrame();
                        var eth = new WiFiFrame();

                        Packet.Make(RouterID, eth, ipv6, tcp);

                        Router temp = FindRoute(Packet);

                        if (temp == null) continue;

                        //temp.ReceiveSNMP(Packet);

                        Task.Factory.StartNew(() => temp.ReceiveSNMP(Packet));
                    }
                    break;
                }
                else
                {
                    Thread.Sleep(100);
                }
            }
        }

        public void ReceiveSNMP(Encapsulation PacketReceived)
        {


            int data = maxAddedBytesPerNode + 16 + 24 + (2*4*4); //w tą i zpowrotem


            var value = PacketReceived.ID < RouterID ? PacketReceived.ID + "-" + RouterID : RouterID + "-" + PacketReceived.ID;
            Stats.Instance.Raport(value, 2*PacketReceived.totalLengthOfFrame + data);


            PacketReceived.goThroughLinkCount++;
            PacketReceived.goThroughLinkDataSum = PacketReceived.goThroughLinkDataSum + 2 * PacketReceived.totalLengthOfFrame + data; //średnio 2kb

            Stats.Instance.RaportExtendedvU(PacketReceived.goThroughLinkCount,
                       PacketReceived.goThroughLinkDataSum, PacketReceived.goThroughAodvCount);

            PacketReceived.goThroughLinkDataSum += Stats.Instance.TBRPData[value];

            if (!PacketReceived.IP_F.DestinationAddress.Equals(this.IPv6))
            {
                Router temp = FindRoute(PacketReceived);
                PacketReceived.ID = this.RouterID;
                temp.ReceiveSNMP(PacketReceived);
            }
            else
            {
                Stats.Instance.nodesToMonitor.AddOrUpdate(PacketReceived.IP_F.DestinationAddress, true, (key, oldValue) => true);
                Stats.Instance.nodesToMonitorTime.AddOrUpdate(PacketReceived.IP_F.DestinationAddress, DateTime.Now,(key, oldValue) => oldValue);

                Stats.Instance.TBRPpacketDataSum.GetOrAdd(Stats.Instance.TBRPpacketDataSum.Count,
                    PacketReceived.goThroughLinkDataSum);

            }          
        }

        private void SendEHBMPvF()
        {
            Thread.Sleep(1000);
            while (true)
            {
                if (isTBRPconvergenced)
                {
                    Stats.Instance.startMonitoringTime = DateTime.Now;

                    //Console.WriteLine("Start Monitoring");

                    var list = ExtensionHeader.DivideTree(TBRP_RoutingTable, maxNodeQuantityToVisit);

                    if (TBRP_RoutingTable.Count == 0)
                    {
                        //Console.WriteLine("Pusta tablica TBRP");
                    }

                    int count = 0;
                    int seed = list.Count;
                    var tasks = new List<Task>();
                    foreach (var item in list)
                    {
                        var Packet = new Encapsulation();
                        var ipv6 = new IPv6Frame();
                        ipv6.DestinationAddress = item.First();
                        item.Remove(item.First());
                        item.Add(IPv6);
                        ipv6.SourceAddress = IPv6;
                        var RoutingExtension = new IPv6_RoutingHeaderFormat(item, item.Count);
                        var eth = new WiFiFrame();
                        eth.SourceMac = MAC;
                        var tcp = new TCPFrame();
                        var monitoringData = new ExtensionHeader();
                        tcp.payLoadLength = 0;

                        if (TBRP_RoutingTable.ContainsKey(ipv6.DestinationAddress))
                        {
                            var temp = TBRP_RoutingTable[ipv6.DestinationAddress].Interface;
                            eth.DestMac = temp.MAC;

                            Packet.Make(RouterID, eth, ipv6, RoutingExtension, tcp, monitoringData);
                            Router.packetID++;
                            tasks.Add(new Task(() => temp.ReceiveEHBMPvF(Packet, monitoringData),
                                TaskCreationOptions.LongRunning));
                            tasks.Last().Start();
                        }

                        count++;

                        if (count == 1)
                        {
                            Task.WaitAll(tasks.ToArray());
                            tasks.Clear();
                            count = 0;
                        }
                    }
                    break;
                }

                Thread.Sleep(100);
            }
        }

        public void ReceiveEHBMPvF(Encapsulation PacketReceived, ExtensionHeader monitoringData)
        {

            Stats.Instance.RaportPacketvFRoute(this.RouterID, Router.packetID);


            PacketReceived.goThroughLinkCount++;
            PacketReceived.goThroughLinkDataSum = PacketReceived.goThroughLinkDataSum +
                                                  PacketReceived.totalLengthOfFrame;


            if (monitoringData != null)
            {
                var value = PacketReceived.ID < RouterID
                    ? PacketReceived.ID + "-" + RouterID
                    : RouterID + "-" + PacketReceived.ID;
                Stats.Instance.Raport(value, PacketReceived.totalLengthOfFrame);
            }

            var count = PacketReceived.IP_Routing.HdrExtLen;
            var tempList = PacketReceived.IP_Routing.Addresses;
            var segmentsLeft = PacketReceived.IP_Routing.SegmentsLeft;

            if (segmentsLeft != 0)
            {
                if (string.Equals(PacketReceived.IP_F.DestinationAddress, IPv6))
                {
                    var RoutingExtension = new IPv6_RoutingHeaderFormat(tempList, count/2);

                    RoutingExtension.SegmentsLeft = segmentsLeft - 1;

                    var Packet = new Encapsulation();
                    var ipv6 = new IPv6Frame();
                    var eth = new WiFiFrame();
                    var tcp = new TCPFrame();

                    ipv6.DestinationAddress = tempList[count/2 - RoutingExtension.SegmentsLeft - 1];

                    ipv6.SourceAddress = PacketReceived.IP_F.SourceAddress;
                    eth.SourceMac = PacketReceived.WiFi_F.SourceMac;

                    monitoringData.listOfData.Add(new ExtensionObject(IPv6, new string('1', 4), 1,
                        new string('1', maxAddedBytesPerNode)));

                    tcp.payLoadLength = monitoringData.TotalLengthOfFrame(2);

                    Packet.Make(RouterID, eth, ipv6, RoutingExtension, tcp, monitoringData);

                    Packet.goThroughLinkCount = PacketReceived.goThroughLinkCount;
                    Packet.goThroughLinkDataSum = PacketReceived.goThroughLinkDataSum;
                    Packet.goThroughAodvCount = PacketReceived.goThroughAodvCount;

                    //przekazac dalej
                    ResendEHBMPvF(Packet, monitoringData);
                }
                else
                {
                    ///przekazac dalej
                    ResendEHBMPvF(PacketReceived, monitoringData);
                }
            }
            else
            {
                if (string.Equals(PacketReceived.IP_F.DestinationAddress, IPv6))
                {
                    Stats.Instance.totalCount += monitoringData.listOfData.Count;

                    foreach (var item in monitoringData.listOfData)
                    {
                        Stats.Instance.nodesToMonitor.AddOrUpdate(item.Address, true, (key, oldValue) => true);

                        Stats.Instance.nodesToMonitorTime.AddOrUpdate(item.Address, DateTime.Now, (key, oldValue) => oldValue);
                    }

                    Stats.Instance.RaportExtendedvF(PacketReceived.goThroughLinkCount,
                        PacketReceived.goThroughLinkDataSum, PacketReceived.goThroughAodvCount);
                }
                else
                {
                    ResendEHBMPvF(PacketReceived, monitoringData);
                }
            }
        }

        private void ResendEHBMPvF(Encapsulation PacketReceived, ExtensionHeader monitoringData)
        {
            var temp = FindRoute(PacketReceived);

            if (temp != null) //Jeśli nie znaleziono routingu to nalezy odrzucic pakiet monitorujacy
            {
                //w przeciwnym wypadku nalezy wyslac pakiet dalej
                temp.ReceiveEHBMPvF(PacketReceived, monitoringData);
            }
            else
            {
                // Console.WriteLine("ODRZUCONO PAKIET MONITORUJACY");
            }
        }


        private void TBRP_PREQ_RoutingProcess()
        {
            while (true)
            {
                Stats.Instance.startRoutingTime = DateTime.Now;

                TBRP_PREQ_Send();

                while (true)
                {
                    cpuCounter.NextValue();
                    Thread.Sleep(1000);
                    int cpu = (int) cpuCounter.NextValue();
                    if (cpu < 20)
                    {
                        break;
                    }
                }
                //Console.WriteLine();
                Stats.Instance.ShowTBRP(TBRP_RoutingTable.Count);
                Stats.Instance.tbrpfinished = true;

                Stats.Instance.startMonitoringTime = DateTime.Now;
                Stats.Instance.TBRPtableCount = TBRP_RoutingTable.Count;

                if (TBRP_RoutingTable.Count != 0)
                {
                    isTBRPconvergenced = true;

                    foreach (var item in nodes)
                    {
                        if (TBRP_RoutingTable.ContainsKey(item.IPv6))
                        {
                            item.isTBRPconvergenced = true;
                            item.newDestination = new UserPacketIPGenerator(IPv6, TBRP_RoutingTable.Keys.ToList());
                        }
                        else
                        {
                            item.stopThread = true;
                        }
                    }
                    break;
                }
                else
                {
                    break;
                }
            }
        }

        private void TBRP_PREQ_Send()
        {
            foreach (var item in nodes)
            {
                item.DiscardTBRP_PREQ = new ConcurrentDictionary<Tuple<int, int>, DateTime>();
            }

            TBRP_PREQ_ID++;
            TBRP_RouterOwnSequenceNumber++;

            var tasks = new List<Task>();

            foreach (var item in Interfaces)
            {
                var rreq = new TBRP_RREQ
                {
                    OriginatorAddress = IPv6,
                    TBRP_RREQ_ID = TBRP_PREQ_ID,
                    OriginatorSequenceNumber = TBRP_RouterOwnSequenceNumber,
                    DestinationSequenceNumber = 0
                };
                var Packet = new Encapsulation();
                var udp = new UDPFrame();

                var ipv6 = new IPv6Frame();
                ipv6.SourceAddress = IPv6;
                ipv6.DestinationAddress = item.Key.IPv6;

                var eth = new WiFiFrame();
                eth.SourceMac = MAC;
                eth.DestMac = item.Key.MAC;

                rreq.DestinationAddress = item.Key.IPv6;
                rreq.Metric = item.Value;
                rreq.HopCount = 0;

                Packet.Make(RouterID, eth, ipv6, udp, rreq);

                tasks.Add(Task.Factory.StartNew(() => item.Key.TBRP_PREQ_Receive(Packet, this)));
            }
        }

        private void TBRP_PREQ_Receive(Encapsulation Packet, Router temp)
        {
            var value = Packet.ID < RouterID ? Packet.ID + "-" + RouterID : RouterID + "-" + Packet.ID;


            Stats.Instance.RaportTBRP(value, Packet.totalLengthOfFrame);
            Stats.Instance.TBRPData_Nodes.AddOrUpdate(RouterID, 1, (key, oldValue) => oldValue + 1);

            RefreshTBRP_RoutingTable();
            var IPOfSenderRREQ = Packet.TBRP_PREQ_F.OriginatorAddress; //To zawsze będzie ROOT
            Packet.TBRP_PREQ_F.HopCount++;

            var tempTableRecord = new TBRP_TableRecord
            {
                DestinationIPv6 = IPOfSenderRREQ,
                Interface = temp,
                DestinationSequenceNumber = Packet.TBRP_PREQ_F.OriginatorSequenceNumber,
                Metric = Packet.TBRP_PREQ_F.Metric,
                HopCount = Packet.TBRP_PREQ_F.HopCount
            };

            if (!string.Equals(IPOfSenderRREQ, IPv6)) //odrzucanie wiadomosci ktore samemu sie wyslalo
            {
                //Uakatualnianie tablicy Routingu
                if (!TBRP_RoutingTable.ContainsKey(IPOfSenderRREQ))
                {
                    TBRP_RoutingTable.GetOrAdd(IPOfSenderRREQ, tempTableRecord);
                    //Wysłanie RREP do ROOT'a
                    TBRP_PREP_Send(IPOfSenderRREQ, temp);
                }
                else
                {
                    if (TBRP_RoutingTable[IPOfSenderRREQ].DestinationSequenceNumber <
                        Packet.TBRP_PREQ_F.OriginatorSequenceNumber)
                    {
                        TBRP_RoutingTable.GetOrAdd(IPOfSenderRREQ, tempTableRecord);
                        TBRP_PREP_Send(IPOfSenderRREQ, temp);
                    }
                    else if (TBRP_RoutingTable[IPOfSenderRREQ].DestinationSequenceNumber ==
                             Packet.TBRP_PREQ_F.OriginatorSequenceNumber)
                    {
                        if (TBRP_RoutingTable[IPOfSenderRREQ].Metric > Packet.TBRP_PREQ_F.Metric)
                        {
                            TBRP_RoutingTable.GetOrAdd(IPOfSenderRREQ, tempTableRecord);
                            //Wysłanie RREP do ROOT'a
                            TBRP_PREP_Send(IPOfSenderRREQ, temp);
                        }
                    }
                }

                //Wysłanie dalej RREQ do sąsiadów
                if (!DiscardTBRP_PREQ.ContainsKey(new Tuple<int, int>(temp.RouterID, Packet.TBRP_PREQ_F.TBRP_RREQ_ID)))
                {
                    TBRP_PREQ_Resend(Packet);
                }

            }
        }

        private void TBRP_PREQ_Resend(Encapsulation PacketRecived)
        {
            var tasks = new List<Task>();

            foreach (var item in Interfaces)
            {
                var Packet = new Encapsulation();
                var udp = PacketRecived.UDP_F;

                var ipv6 = new IPv6Frame();
                ipv6.SourceAddress = IPv6;
                ipv6.DestinationAddress = item.Key.IPv6;

                var eth = PacketRecived.WiFi_F;
                eth.SourceMac = MAC;
                eth.DestMac = item.Key.MAC;

                var rreq = new TBRP_RREQ
                {
                    OriginatorSequenceNumber = PacketRecived.TBRP_PREQ_F.OriginatorSequenceNumber,
                    TBRP_RREQ_ID = PacketRecived.TBRP_PREQ_F.TBRP_RREQ_ID
                };
                rreq.Metric = PacketRecived.TBRP_PREQ_F.Metric + item.Value;
                rreq.DestinationAddress = item.Key.IPv6;
                rreq.DestinationSequenceNumber = PacketRecived.TBRP_PREQ_F.DestinationSequenceNumber;
                rreq.HopCount = PacketRecived.TBRP_PREQ_F.HopCount;
                rreq.OriginatorAddress = PacketRecived.TBRP_PREQ_F.OriginatorAddress;

                Packet.Make(RouterID, eth, ipv6, udp, rreq);

                var MinimalLifeTime = DateTime.Now;
                MinimalLifeTime = MinimalLifeTime.AddMilliseconds(PATH_DISCOVERY_TIME);

                DiscardTBRP_PREQ.GetOrAdd(
                    new Tuple<int, int>(item.Key.RouterID, PacketRecived.TBRP_PREQ_F.TBRP_RREQ_ID), MinimalLifeTime);

                tasks.Add(Task.Factory.StartNew(() => item.Key.TBRP_PREQ_Receive(Packet, this)));
            }
        }

        public void TBRP_PREP_Receive(Encapsulation Packet)
        {
            var value = Packet.ID < RouterID ? Packet.ID + "-" + RouterID : RouterID + "-" + Packet.ID;

            Stats.Instance.RaportTBRP(value, Packet.totalLengthOfFrame);

            RefreshTBRP_RoutingTable();
            var IPOfSenderRREP = Packet.TBRP_PREP_F.OriginatorAddress;
            Packet.TBRP_PREP_F.HopCount++;

            Router temp = null;
            foreach (var item in Interfaces) //szukanie od kogo pochodzil RREP
            {
                if (string.Equals(item.Key.IPv6, Packet.IP_F.SourceAddress))
                {
                    temp = item.Key;
                    break;
                }
            }

            var tempTableRecord = new TBRP_TableRecord
            {
                DestinationIPv6 = IPOfSenderRREP,
                DestinationSequenceNumber = Packet.TBRP_PREP_F.DestinationSequenceNumber,
                Interface = temp,
                Metric = Packet.TBRP_PREP_F.Metric,
                HopCount = Packet.TBRP_PREP_F.HopCount
            };

            if (!TBRP_RoutingTable.ContainsKey(IPOfSenderRREP)) //zapis danych od nadawcy
            {
                TBRP_RoutingTable.GetOrAdd(IPOfSenderRREP, tempTableRecord);
            }
            else
            {
                if (TBRP_RoutingTable[IPOfSenderRREP].DestinationSequenceNumber <
                    Packet.TBRP_PREP_F.DestinationSequenceNumber)
                {
                    TBRP_RoutingTable.GetOrAdd(IPOfSenderRREP, tempTableRecord);
                }
                else if (TBRP_RoutingTable[IPOfSenderRREP].DestinationSequenceNumber ==
                         Packet.TBRP_PREP_F.DestinationSequenceNumber)
                {
                    if (TBRP_RoutingTable[IPOfSenderRREP].Metric > Packet.TBRP_PREP_F.Metric)
                    {
                        TBRP_RoutingTable.GetOrAdd(IPOfSenderRREP, tempTableRecord);
                    }
                }
            }

            if (!string.Equals(Packet.TBRP_PREP_F.DestinationAddress, IPv6))
            {
                TBRP_PREP_Resend(Packet);
            }
        }

        public void TBRP_PREP_Send(string IPOfSenderRREQ, Router temp)
        {
            TBRP_RouterOwnSequenceNumber++;

            var Packet = new Encapsulation();
            var udp = new UDPFrame();

            var ipv6 = new IPv6Frame();
            ipv6.SourceAddress = IPv6;

            var eth = new WiFiFrame();
            eth.SourceMac = MAC;

            var rrep = new TBRP_RREP
            {
                DestinationAddress = IPOfSenderRREQ,
                DestinationSequenceNumber = TBRP_RouterOwnSequenceNumber,
                OriginatorAddress = IPv6
            };

            eth.DestMac = temp.MAC;
            ipv6.DestinationAddress = temp.IPv6;

            rrep.Metric = Interfaces[temp];
            rrep.HopCount = 0;

            Packet.Make(RouterID, eth, ipv6, udp, rrep);

            Task.Run(() => temp.TBRP_PREP_Receive(Packet));
        }

        public void TBRP_PREP_Resend(Encapsulation PacketRecived)
        {
            Router temp = null;

            var Packet = new Encapsulation();
            var udp = new UDPFrame();

            var ipv6 = new IPv6Frame();
            ipv6.SourceAddress = IPv6;

            var eth = new WiFiFrame();

            eth.SourceMac = MAC;

            var rrep = new TBRP_RREP
            {
                DestinationAddress = PacketRecived.TBRP_PREP_F.DestinationAddress,
                DestinationSequenceNumber = PacketRecived.TBRP_PREP_F.DestinationSequenceNumber,
                OriginatorAddress = PacketRecived.TBRP_PREP_F.OriginatorAddress
            };

            temp = TBRP_RoutingTable[PacketRecived.TBRP_PREP_F.DestinationAddress].Interface;
            eth.DestMac = temp.MAC;
            ipv6.DestinationAddress = temp.IPv6;

            rrep.Metric = PacketRecived.TBRP_PREP_F.Metric + Interfaces[temp];
            rrep.HopCount = PacketRecived.TBRP_PREP_F.HopCount++;

            Packet.Make(RouterID, eth, ipv6, udp, rrep);

            Task.Run(() => temp.TBRP_PREP_Receive(Packet));
        }

        /*private void userAplicationSimulatedProcess()
        {
            Thread.Sleep(1000);
            var randomize = new Random(DateTime.Now.Millisecond);
            while (true)
            {
                if (stopThread == false)
                {
                    if (isTBRPconvergenced && isMonitored == false)
                    {
                        //Task.Delay(randomize.Next(100, 1000));
                        this.time_ms += 1000;
                        SendUserPacket();
                    }
                    if (isMonitored)
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
        }*/

        private void userAplicationSimulatedProcess()
        {
            //watek główny
            Thread.Sleep(1000);

            while (true)
            {
                if (isTBRPconvergenced && isMonitored == false)
                {
                    foreach (var item in nodes.Skip(1))
                    {
                        if (TBRP_RoutingTable.ContainsKey(item.IPv6))
                        {
                            item.time_ms += timeIntervalUms;
                            item.SendUserPacket();
                           // Task.Factory.StartNew(() => item.SendUserPacket());
                        }                     
                    }
                }
                else
                {
                    Thread.Sleep(100);
                }
                if (isMonitored)
                {
                    int maxTime = 0;
                    foreach (var item in nodes.Skip(1))
                    {
                        if (maxTime < item.time_ms)
                        {
                            maxTime = item.time_ms;
                        }
                    }

                    Stats.Instance.timeMS = (float) maxTime/1000;
                    break;
                }

               
            }
        }


        public void ReceiveUserPacket(Encapsulation PacketReceived)
        {
            PacketReceived.goThroughLinkCount++;
            PacketReceived.goThroughLinkDataSum = PacketReceived.goThroughLinkDataSum + 2000; //średnio 2kb

            if (PacketReceived.Extension != null)
            {
                var value = PacketReceived.ID < RouterID
                    ? PacketReceived.ID + "-" + RouterID
                    : RouterID + "-" + PacketReceived.ID;

                var tempCount = 0;

                foreach (var item in PacketReceived.Extension.listOfData)
                {
                    if (Stats.Instance.nodesToMonitor[item.Address])
                    {
                        tempCount++;
                    }
                }

                var temp = (PacketReceived.Extension.listOfData.Count - tempCount)*ExtensionObject.LengthOfObjectM1;

                if (temp != 0)
                {
                    temp += 4;
                }

                Stats.Instance.Raport(value, temp);
            }

            if (string.Equals(PacketReceived.IP_F.DestinationAddress, IPv6))
            {
                //Jesteś u Celu
                if (PacketReceived.Extension != null)
                {
                    foreach (var item in PacketReceived.Extension.listOfData)
                    {
                        Stats.Instance.nodesToMonitor.AddOrUpdate(item.Address, true, (key, oldValue) => true);
                        Stats.Instance.nodesToMonitorTime.AddOrUpdate(item.Address, DateTime.Now,
                            (key, oldValue) => oldValue);
                    }
                    Stats.Instance.totalCount += PacketReceived.Extension.listOfData.Count;
                    Stats.Instance.RaportExtendedvU(PacketReceived.goThroughLinkCount,
                        PacketReceived.goThroughLinkDataSum, PacketReceived.goThroughAodvCount);
                }
            }
            else
            {
                if (PacketReceived.IP_F.DestinationAddress == nodes[0].IPv6 && needSendData)
                {
                    if (PacketReceived.Extension != null)
                    {
                        var lengthOfTcpPayload = PacketReceived.TCP_F.payLoadLength;
                        var monitoringdataLength = PacketReceived.Extension.TotalLengthOfFrame(1);

                        if (PacketReceived.TCP_F.lengthOfHeader + lengthOfTcpPayload +
                            PacketReceived.IP_F.lengthOfHeader + ExtensionObject.LengthOfObjectM1 + monitoringdataLength <=
                            payLoadWifi)
                        {
                            PacketReceived.Extension.listOfData.Add(new ExtensionObject(IPv6, new string('1', 4), 1,
                                new string('1', maxAddedBytesPerNode)));
                            PacketReceived.Extension.quantityOfSegments++;
                        }


                        var Packet = new Encapsulation();
                        Packet.Make(RouterID, PacketReceived.WiFi_F, PacketReceived.IP_F, PacketReceived.Extension,
                            PacketReceived.TCP_F);

                        Packet.goThroughLinkCount = PacketReceived.goThroughLinkCount;
                        Packet.goThroughLinkDataSum = PacketReceived.goThroughLinkDataSum;
                        Packet.goThroughAodvCount = PacketReceived.goThroughAodvCount;

                        //needSendData = false;
                        ResendUserPacket(Packet);
                    }
                    else
                    {
                        PacketReceived.ID = RouterID;
                        ResendUserPacket(PacketReceived);
                    }
                }
                else
                {
                    PacketReceived.ID = RouterID;
                    ResendUserPacket(PacketReceived);
                }
            }
        }

        public void SetNeedSendMonitoringData(object source, ElapsedEventArgs e)
        {
            needSendData = true;
        }

        private void SetRouterMonitoringTimer()
        {
            var timerandom = new Random();
            var monitoringTimer = new Timer();
            monitoringTimer.Interval = timerandom.Next(1000, 5000);
            monitoringTimer.Elapsed += SetNeedSendMonitoringData;
            monitoringTimer.AutoReset = true;
            monitoringTimer.Enabled = true;
        }

        private void ResendUserPacket(Encapsulation PacketReceived)
        {
            Router temp;
            temp = FindRoute(PacketReceived);

            if (temp != null)
            {
                temp.ReceiveUserPacket(PacketReceived);
            }
        }

        private bool SendUserPacket()
        {
            bool flag = false;

            var randomize = new Random(DateTime.Now.Millisecond);
            Router temp;
            var Packet = new Encapsulation();
            var tcp = new TCPFrame();
            var ipv6 = new IPv6Frame();
            var eth = new WiFiFrame();

            ipv6.SourceAddress = IPv6;
            eth.SourceMac = MAC;

            var lengthOfTcpPayload = randomize.Next(1, payLoadWifi); //w bajtach - losowanie rozmiaru danych
            tcp.payLoadLength = lengthOfTcpPayload;

            ExtensionHeader monitoring;
            monitoring = null;

            if (randomize.Next(0, 101) <= -1)
            {
                ipv6.DestinationAddress = newDestination.RandomDestination();
            }
            else
            {
                ipv6.DestinationAddress = nodes[0].IPv6;
            }

            //jeśli pakiet jest adresowany do centrum monitoringu i potrzeba monitoringu
            if (ipv6.DestinationAddress == nodes[0].IPv6 && needSendData)
            {
                if (tcp.lengthOfHeader + lengthOfTcpPayload + ipv6.lengthOfHeader + ExtensionObject.LengthOfObjectM1 + 4 <=
                    payLoadWifi)
                {
                    monitoring = new ExtensionHeader();
                    //dodajemy dane monitoringu
                    monitoring.quantityOfSegments++;
                    monitoring.listOfData.Add(new ExtensionObject(IPv6, new string('1', 4), 1,
                        new string('1', maxAddedBytesPerNode))); //wypelnienie w bajtach
                    //needSendData = false;
                    Packet.Make(RouterID, eth, ipv6, monitoring, tcp);
                }
                else
                {
                    Packet.Make(RouterID, eth, ipv6, tcp);
                    flag = true;
                }
            }
            else
            {
                Packet.Make(RouterID, eth, ipv6, tcp);
            }

            temp = FindRoute(Packet);

            if (temp != null)
            {
                temp.ReceiveUserPacket(Packet);
            }

            if (flag == false)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private Router FindRoute(Encapsulation Packet)
        {
            Packet.WiFi_F.DestMac = null;
            var ipv6 = Packet.IP_F.DestinationAddress;
            Router temp;


            var randomize = new Random(DateTime.Now.Millisecond);

            if ((randomize.Next(0, 101)) <= 5 && (choise != MonitoringType.Forced) && Router.isaccidentposible == true)
            {
                Program.accident = true;
            }


            if (Program.accident == false)
            {
                foreach (var item in Interfaces) //sprawdzanie sąsiadów
                {
                    if (string.Equals(item.Key.IPv6, ipv6))
                    {
                        Packet.WiFi_F.DestMac = item.Key.MAC;
                        temp = item.Key;

                        return temp;
                    }
                }

                if (TBRP_RoutingTable.ContainsKey(ipv6)) //Sprawdzanie Tablicy Routingu TBRP
                {
                    temp = TBRP_RoutingTable[ipv6].Interface;
                    Packet.WiFi_F.DestMac = temp.MAC;
                    return temp;
                }

                RefreshAODV_RoutingTable();
                if (this.AODV_RoutingTable.ContainsKey(ipv6)) //Sprawdzanie Tablicy Routingu AODV
                {
                    temp = AODV_RoutingTable[ipv6].Interface;
                    Packet.WiFi_F.DestMac = temp.MAC;
                    return temp;
                }

                if (SNMP == true)
                {
                    return null;
                }
            }
            else
            {
                Program.accident = false;
                Stats.Instance.LinkAccidentReport();
            }

            for (var i = 0; i < 6; i++)
            {
                AODV_RREQ_Send(ipv6);
                Stats.Instance.AODVcount++;

                int count = 0;
                while (true)
                {
                    if (this.AODV_RoutingTable.ContainsKey(ipv6))
                    {
                        break;
                    }
                    else
                    {
                        Thread.Sleep(10);
                        count++;
                        if (count == 2000)
                        {
                            count = 0;
                            break;
                        }
                    }
                }

                if (this.AODV_RoutingTable.ContainsKey(ipv6))
                {
                    temp = AODV_RoutingTable[ipv6].Interface;
                    Packet.WiFi_F.DestMac = temp.MAC;

                    while (true)
                    {
                        cpuCounter.NextValue();
                        Thread.Sleep(100);
                        int cpu = (int) cpuCounter.NextValue();

                        if (cpu < 20)
                        {
                            break;
                        }
                    }

                    Packet.goThroughAodvCount++;

                    return temp;
                }
                Thread.Sleep(3000 + (i + 1)*1000);
            }

            return null;
        }

        private void AODV_RREQ_Send(string PacketDestinationAddress)
        {
            foreach (var item in nodes)
            {
                item.DiscardAODV_RREQ = new ConcurrentDictionary<Tuple<int, int>, DateTime>();
            }

            var DestSeqNumber = 0;

            AODV_RREQ_ID++;
            AODV_RouterOwnSequenceNumber++;

            var rreq = new AODV_RREQ
            {
                HopC = 0,
                RREQ_ID = AODV_RREQ_ID,
                OriginatorAddress = IPv6,
                OriginatorSequenceNumber = AODV_RouterOwnSequenceNumber,
                DestinationAddress = PacketDestinationAddress,
                DestinationSequenceNumber = DestSeqNumber,
                TTL = nodesCount
            };

            foreach (var item in Interfaces)
            {
                var Packet = new Encapsulation();
                var udp = new UDPFrame();

                var ipv6 = new IPv6Frame();
                ipv6.SourceAddress = IPv6;
                ipv6.DestinationAddress = item.Key.IPv6;

                var eth = new WiFiFrame();
                eth.SourceMac = MAC;
                eth.DestMac = item.Key.MAC;


                rreq.Metric = item.Value;

                Packet.Make(RouterID, eth, ipv6, udp, rreq);
                Task.Factory.StartNew(() => item.Key.AODV_RREQ_Receive(Packet, this));
            }
        }

        private void AODV_RREQ_Resend(Encapsulation PacketRecived)
        {
            var tasks = new List<Task>();

            foreach (var item in Interfaces)
            {
                var Packet = new Encapsulation();
                var udp = new UDPFrame();

                var ipv6 = new IPv6Frame();
                ipv6.SourceAddress = IPv6;
                ipv6.DestinationAddress = item.Key.IPv6;

                var eth = new WiFiFrame();
                eth.SourceMac = MAC;
                eth.DestMac = item.Key.MAC;

                var rreq = new AODV_RREQ
                {
                    DestinationAddress = PacketRecived.AODV_RREQ_F.DestinationAddress,
                    DestinationSequenceNumber = PacketRecived.AODV_RREQ_F.DestinationSequenceNumber,
                    HopC = PacketRecived.AODV_RREQ_F.HopC,
                    OriginatorAddress = PacketRecived.AODV_RREQ_F.OriginatorAddress,
                    OriginatorSequenceNumber = PacketRecived.AODV_RREQ_F.OriginatorSequenceNumber,
                    RREQ_ID = PacketRecived.AODV_RREQ_F.RREQ_ID,
                    TTL = PacketRecived.AODV_RREQ_F.TTL - 1
                };

                rreq.Metric = PacketRecived.AODV_RREQ_F.Metric + item.Value;

                Packet.Make(RouterID, eth, ipv6, udp, rreq);

                var MinimalLifeTime = DateTime.Now;
                MinimalLifeTime = MinimalLifeTime.AddMilliseconds(100*PATH_DISCOVERY_TIME);

                DiscardAODV_RREQ.GetOrAdd(new Tuple<int, int>(item.Key.RouterID, PacketRecived.AODV_RREQ_F.RREQ_ID),
                    MinimalLifeTime);

                tasks.Add(Task.Factory.StartNew(() => item.Key.AODV_RREQ_Receive(Packet, this)));
            }
        }

        public void AODV_RREQ_Receive(Encapsulation Packet, Router temp)
        {
            var value = Packet.ID < RouterID ? Packet.ID + "-" + RouterID : RouterID + "-" + Packet.ID;
            Stats.Instance.RaportAODV(value, Packet.totalLengthOfFrame);

            if (Packet.AODV_RREQ_F.TTL == 0)
            {
                return;
            }

            RefreshAODV_RoutingTable();
            var IPOfSenderRREQ = Packet.AODV_RREQ_F.OriginatorAddress;

            if (!string.Equals(IPOfSenderRREQ, IPv6)) //odrzucanie wiadomosci ktore samemu sie wyslalo
            {
                Packet.AODV_RREQ_F.HopC++;

                var MinimalLifeTime = DateTime.Now;

                var timeinmiliseconds = MY_ROUTE_TIMEOUT;
                MinimalLifeTime = MinimalLifeTime.AddMilliseconds(timeinmiliseconds);

                var tempTableRecord = new AODV_TableRecord
                {
                    DestinationIPv6 = IPOfSenderRREQ,
                    HopCount = Packet.AODV_RREQ_F.HopC,
                    DestinationSequenceNumber = Packet.AODV_RREQ_F.OriginatorSequenceNumber,
                    Interface = temp,
                    LifeTime = MinimalLifeTime,
                    Metric = Packet.AODV_RREQ_F.Metric
                };


                if (!AODV_RoutingTable.ContainsKey(IPOfSenderRREQ)) //zapis danych od nadawcy
                {
                    AODV_RoutingTable.GetOrAdd(IPOfSenderRREQ, tempTableRecord);
                }
                else
                {
                    if (AODV_RoutingTable[IPOfSenderRREQ].DestinationSequenceNumber <
                        Packet.AODV_RREQ_F.OriginatorSequenceNumber)
                    {
                        AODV_RoutingTable.GetOrAdd(IPOfSenderRREQ, tempTableRecord);
                    }
                    else if (AODV_RoutingTable[IPOfSenderRREQ].DestinationSequenceNumber ==
                             Packet.AODV_RREQ_F.OriginatorSequenceNumber)
                    {
                        if (AODV_RoutingTable[IPOfSenderRREQ].Metric > Packet.AODV_RREQ_F.Metric)
                        {
                            AODV_RoutingTable.GetOrAdd(IPOfSenderRREQ, tempTableRecord);
                        }
                    }
                }

                if (string.Equals(Packet.AODV_RREQ_F.DestinationAddress, IPv6)) //Jest Router jest szukanym Routerem
                {
                    AODV_RREP_Send(Packet.AODV_RREQ_F.OriginatorAddress, temp); //Wysłanie RREP na adres nadawcy RREQ
                }
                else if (AODV_RoutingTable.ContainsKey(Packet.AODV_RREQ_F.DestinationAddress))
                    //Jeśli Router posiada informację o trasie do Routera Destination
                {
                    if (AODV_RoutingTable[Packet.AODV_RREQ_F.DestinationAddress].DestinationSequenceNumber <=
                        Packet.AODV_RREQ_F.DestinationSequenceNumber)
                    {
                        AODV_RREP_SendFromIntermediateNode(Packet.AODV_RREQ_F.OriginatorAddress,
                            Packet.AODV_RREQ_F.DestinationAddress);
                    }
                    else
                    {
                        if (
                            !DiscardAODV_RREQ.ContainsKey(new Tuple<int, int>(temp.RouterID, Packet.AODV_RREQ_F.RREQ_ID)))
                        {
                            AODV_RREQ_Resend(Packet);
                        }
                    }
                }
                else
                {
                    if (!DiscardAODV_RREQ.ContainsKey(new Tuple<int, int>(temp.RouterID, Packet.AODV_RREQ_F.RREQ_ID)))
                    {
                        AODV_RREQ_Resend(Packet);
                    }
                }
            }
        }

        public void AODV_RREP_SendFromIntermediateNode(string OrginatorOfRREQ, string DestinationOfRREQ)
        {
            Router temp = null;
            var Packet = new Encapsulation();
            var udp = new UDPFrame();

            var ipv6 = new IPv6Frame();
            ipv6.SourceAddress = IPv6;

            var eth = new WiFiFrame();
            eth.SourceMac = MAC;

            var HopCount = 0;
            var DestSeqNumber = 0;

            if (AODV_RoutingTable.ContainsKey(DestinationOfRREQ))
            {
                HopCount = AODV_RoutingTable[DestinationOfRREQ].HopCount;
                DestSeqNumber = AODV_RoutingTable[DestinationOfRREQ].DestinationSequenceNumber;


                var ExpirationTime = AODV_RoutingTable[DestinationOfRREQ].LifeTime;
                var TimeInMilisecond = (int) ExpirationTime.Subtract(DateTime.Now).TotalMilliseconds;

                var rrep = new AODV_RREP
                {
                    HopC = HopCount,
                    LifeTime = TimeInMilisecond,
                    DestinationAddress = OrginatorOfRREQ,
                    DestinationSequenceNumber = DestSeqNumber,
                    OriginatorAddress = DestinationOfRREQ
                };


                temp = AODV_RoutingTable[OrginatorOfRREQ].Interface;

                rrep.Metric = Interfaces[temp];

                Packet.Make(RouterID, eth, ipv6, udp, rrep);
                Task.Factory.StartNew(() => temp.AODV_RREP_Receive(Packet, this));
            }
        }

        public void AODV_RREP_Send(string OrginatorOfRREQ, Router temp)
        {
            AODV_RouterOwnSequenceNumber++;
            var Packet = new Encapsulation();
            var udp = new UDPFrame();

            var ipv6 = new IPv6Frame();
            ipv6.SourceAddress = IPv6;

            var eth = new WiFiFrame();
            eth.SourceMac = MAC;

            var rrep = new AODV_RREP
            {
                HopC = 0,
                LifeTime = MY_ROUTE_TIMEOUT,
                DestinationAddress = OrginatorOfRREQ,
                DestinationSequenceNumber = AODV_RouterOwnSequenceNumber,
                OriginatorAddress = IPv6
            };

            eth.DestMac = temp.MAC;
            ipv6.DestinationAddress = temp.IPv6;
            rrep.Metric = Interfaces[temp];

            Packet.Make(RouterID, eth, ipv6, udp, rrep);

            Task.Run(() => temp.AODV_RREP_Receive(Packet, this));
        }

        public void AODV_RREP_Resend(Encapsulation PacketRecived)
        {
            Router temp = null;

            var Packet = new Encapsulation();
            var udp = new UDPFrame();

            var ipv6 = new IPv6Frame();
            ipv6.SourceAddress = IPv6;

            var eth = new WiFiFrame();

            eth.SourceMac = MAC;

            var rrep = new AODV_RREP
            {
                HopC = PacketRecived.AODV_RREP_F.HopC,
                LifeTime = MY_ROUTE_TIMEOUT,
                DestinationAddress = PacketRecived.AODV_RREP_F.DestinationAddress,
                DestinationSequenceNumber = PacketRecived.AODV_RREP_F.DestinationSequenceNumber,
                OriginatorAddress = PacketRecived.AODV_RREP_F.OriginatorAddress
            };

            if (AODV_RoutingTable.ContainsKey(PacketRecived.AODV_RREP_F.DestinationAddress))
            {
                temp = AODV_RoutingTable[PacketRecived.AODV_RREP_F.DestinationAddress].Interface;
                eth.DestMac = temp.MAC;
                ipv6.DestinationAddress = temp.IPv6;

                rrep.Metric = Interfaces[temp] + PacketRecived.AODV_RREP_F.Metric;

                Packet.Make(RouterID, eth, ipv6, udp, rrep);

                Task.Run(() => temp.AODV_RREP_Receive(Packet, this));
            }
        }

        public void AODV_RREP_Receive(Encapsulation Packet, Router temp)
        {
            var value = Packet.ID < RouterID ? Packet.ID + "-" + RouterID : RouterID + "-" + Packet.ID;
            Stats.Instance.RaportAODV(value, Packet.totalLengthOfFrame);

            RefreshAODV_RoutingTable();
            var IPOfSenderRREP = Packet.AODV_RREP_F.OriginatorAddress;

            Packet.AODV_RREP_F.HopC++;

            var MinimalLifeTime = DateTime.Now;
            MinimalLifeTime = MinimalLifeTime.AddMilliseconds(Packet.AODV_RREP_F.LifeTime);


            var tempTableRecord = new AODV_TableRecord
            {
                DestinationIPv6 = IPOfSenderRREP,
                LifeTime = MinimalLifeTime,
                HopCount = Packet.AODV_RREP_F.HopC,
                DestinationSequenceNumber = Packet.AODV_RREP_F.DestinationSequenceNumber,
                Interface = temp,
                Metric = Packet.AODV_RREP_F.Metric
            };

            if (!AODV_RoutingTable.ContainsKey(IPOfSenderRREP)) //zapis danych od nadawcy
            {
                AODV_RoutingTable.GetOrAdd(IPOfSenderRREP, tempTableRecord);
            }
            else
            {
                if (AODV_RoutingTable[IPOfSenderRREP].DestinationSequenceNumber <
                    Packet.AODV_RREP_F.DestinationSequenceNumber)
                {
                    AODV_RoutingTable.GetOrAdd(IPOfSenderRREP, tempTableRecord);
                }
                else if (AODV_RoutingTable[IPOfSenderRREP].DestinationSequenceNumber ==
                         Packet.AODV_RREP_F.DestinationSequenceNumber)
                {
                    if (AODV_RoutingTable[IPOfSenderRREP].Metric > Packet.AODV_RREP_F.Metric)
                    {
                        AODV_RoutingTable.GetOrAdd(IPOfSenderRREP, tempTableRecord);
                    }
                }
            }

            if (!string.Equals(Packet.AODV_RREP_F.DestinationAddress, IPv6))
            {
                AODV_RREP_Resend(Packet);
            }
        }

        private void RefreshAODV_RoutingTable()
        {
            var time = DateTime.Now;

            var Temp2 = new Dictionary<Tuple<int, int>, DateTime>(DiscardAODV_RREQ);

            foreach (var item in Temp2)
            {
                if (DateTime.Compare(item.Value, time) < 0)
                {
                    DateTime abc;
                    DiscardAODV_RREQ.TryRemove(item.Key, out abc);
                }
            }

            foreach (var item in AODV_RoutingTable)
            {
                if (DateTime.Compare(item.Value.LifeTime, time) < 0)
                {
                    AODV_TableRecord abc;
                    AODV_RoutingTable.TryRemove(item.Key, out abc);
                }
            }
        }

        private void RefreshTBRP_RoutingTable()
        {
            var time = DateTime.Now;
            var Temp2 = new Dictionary<Tuple<int, int>, DateTime>(DiscardTBRP_PREQ);

            foreach (var item in Temp2)
            {
                if (DateTime.Compare(item.Value, time) < 0)
                {
                    DateTime abc;
                    DiscardTBRP_PREQ.TryRemove(item.Key, out abc);
                }
            }
        }

        public void AddInterface(Router node, int metrics)
        {
            Interfaces.GetOrAdd(node, metrics);
        }
    }
}