﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Symulator
{
    /// <summary>
    /// Interaction logic for API.xaml
    /// </summary>
    public partial class API : Window
    {
        public string toFileString { get; set; } = null;
        private int nodesCount = 0;
        private int linkCount = 0;
        private Dictionary<string, int> LinkList;
        private Dictionary<int, Tuple<string, string>> dictWithUniqNodesRef;

        public API()
        {
            InitializeComponent();

            accesstoken.Text = "iFowY3GF1qpsJMzTmvym3glKB7cmE7";
            //accesstoken.Text = "";
            szer.Text = "54.94607";
            dl.Text = "17.96539";
            radius.Text = "3000";
            Srok.Text = "2010";
            Smies.Text = "11";
            Sdzie.Text = "19";
           // Erok.Text = "2015";
          //  Emies.Text = "6";
          //  Edzie.Text = "30";
            Sgodz.Text = "0";
         //   Egodz.Text = "17";
            Smin.Text = "0";
        //    Emin.Text = "23";

            button_load.IsEnabled = false;
            button_save.IsEnabled = false;

            LinkList = new Dictionary<string, int>();
            dictWithUniqNodesRef = new Dictionary<int, Tuple<string, string>>();
        }

        public string ConvertWithZero(int value)
        {
            string temp;
            if (value < 10)
            {
                temp = "0" + value;
            }
            else
            {
                temp = "" + value;
            }

            return temp;
        }

        private void Api_Load(object sender, RoutedEventArgs e)
        {
            double szerokosc;
            double dlugosc;
            int promien;
            int StartRok;
            int KoniecRok;
            int StartMiesiac;
            int KoniecMiesiac;
            int StartDzien;
            int KoniecDzien;
            int StartGodzina;
            int KoniecGodzina;
            int StartMinuta;
            int KoniecMinuta;

            string dateStart;
            string dateStop;

            bool error = false;

            double.TryParse(szer.Text.Replace('.', ','), out szerokosc);
            if (szerokosc == 0)
            {
                error = true;
            }
            double.TryParse(dl.Text.Replace('.', ','), out dlugosc);
            if (dlugosc == 0)
            {
                error = true;
            }
            int.TryParse(radius.Text.Replace('.', ','), out promien);
            if ((promien <= 0) || (promien > 25000))
            {
                if (promien > 25000)
                {
                    MessageBox.Show("Maksymalny promień to 25000!", "Błąd", MessageBoxButton.OK, MessageBoxImage.Warning);
                }

                error = true;
            }
            int.TryParse(Srok.Text.Replace('.', ','), out StartRok);
            if ((StartRok <= 0) || (StartRok > 3000))
            {
                error = true;
            }
          /*  int.TryParse(Erok.Text.Replace('.', ','), out KoniecRok);
            if ((KoniecRok <= 0) || (KoniecRok > 3000))
            {
                error = true;
            }*/
            int.TryParse(Smies.Text.Replace('.', ','), out StartMiesiac);
            if ((StartMiesiac <= 0) || (StartMiesiac > 13))
            {
                error = true;
            }
          /*  int.TryParse(Emies.Text.Replace('.', ','), out KoniecMiesiac);
            if ((KoniecMiesiac <= 0) || (KoniecMiesiac > 13))
            {
                error = true;
            }*/
            int.TryParse(Sdzie.Text.Replace('.', ','), out StartDzien);
            if ((StartDzien <= 0) || (StartDzien > 32))
            {
                error = true;
            }
            /*int.TryParse(Edzie.Text.Replace('.', ','), out KoniecDzien);
            if ((KoniecDzien <= 0) || (KoniecDzien > 32))
            {
                error = true;
            }*/
            int.TryParse(Sgodz.Text.Replace('.', ','), out StartGodzina);
            if ((StartGodzina < 0) || (StartGodzina > 23))
            {
                error = true;
            }
            /*int.TryParse(Egodz.Text.Replace('.', ','), out KoniecGodzina);
            if ((KoniecGodzina < 0) || (KoniecGodzina > 23))
            {
                error = true;
            }*/
            int.TryParse(Smin.Text.Replace('.', ','), out StartMinuta);
            if ((StartMinuta < 0) || (StartMinuta > 59))
            {
                error = true;
            }
            /*int.TryParse(Emin.Text.Replace('.', ','), out KoniecMinuta);
            if ((KoniecMinuta < 0) || (KoniecMinuta > 59))
            {
                error = true;
            }*/


            if (error == true)
            {
                MessageBox.Show("Wprowadzono błędne dane!", "Błąd", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
                return;
            }

            dateStart = "datetime_start=" + ConvertWithZero(StartRok) + "-" + ConvertWithZero(StartMiesiac) + "-" +
                        ConvertWithZero(StartDzien) + "T";
            dateStart += ConvertWithZero(StartGodzina) + ":" + ConvertWithZero(StartMinuta) + ":00Z";



            DateTime time = new DateTime(StartRok, StartMiesiac, StartDzien, StartGodzina, StartMinuta, 0);

            time = time.AddHours(1);



             dateStop = "datetime_end=" + ConvertWithZero(time.Year) + "-" + ConvertWithZero(time.Month) + "-" +
                         ConvertWithZero(time.Day) + "T";
             dateStop += ConvertWithZero(time.Hour) + ":" + ConvertWithZero(time.Minute) + ":00Z";
             



            string szerS = ((float) szerokosc) + "";

            string dlS = ((float) dlugosc) + "";

            string location = "location=" + szerS.Replace(',', '.') + "," + dlS.Replace(',', '.') + "," + promien;


            string accessToken = "access_token=" + accesstoken.Text;

            string sURL = "http://ais.kti.gda.pl/api/v1/ships/positions?" + dateStart + "&" + dateStop + "&" +
                            location + "&" + accessToken;

            WebRequest wrGETURL;
            wrGETURL = WebRequest.Create(sURL);
            //wrGETURL.Method = "GET";
            Stream objStream;
            StreamReader objReader = null;
            Mouse.OverrideCursor = Cursors.Wait;
            try
            {
                objStream = wrGETURL.GetResponse().GetResponseStream();
                objReader = new StreamReader(objStream);
            }
            catch (Exception)
            {
                label_status.Content = "STATUS: Błąd! Zły Access Token lub wprowadzono błędnie dane!";
                Mouse.OverrideCursor = Cursors.Arrow;
                return;
            }

            string sLine = "";
            string txt = null;

            while (sLine != null)
            {
                sLine = objReader.ReadLine();
                if (sLine != null)
                    txt += sLine;
            }                   

            var list = new List<Tuple<string, string>>();

            string[] abc = txt.Split(' ');
            int counter = 0;
            foreach (var item in abc)
            {
                if (item.IndexOf("coordinates") > 0)
                {
                    string[] first = abc[counter + 1].Split('.');
                    string[] second = abc[counter + 2].Split('.');

                    var resultfirst = Regex.Match(first[0], @"\d+").Value + "," + Regex.Match(first[1], @"\d+").Value;
                    var resultsecond = Regex.Match(second[0], @"\d+").Value + "," + Regex.Match(second[1], @"\d+").Value;

                    list.Add(new Tuple<string, string>(resultfirst, resultsecond));
                }
                counter++;
            }

            if (list.Count == 0)
            {
                label_status.Content = "STATUS: W podanym obszarze, w podanym czasie nic nie znaleziono!";
                Mouse.OverrideCursor = Cursors.Arrow;
                return;
            }

            var dictWithUniqNodes = new Dictionary<Tuple<string, string>, int>();

            int ID = 0;

            LinkList = new Dictionary<string, int>();
            dictWithUniqNodesRef = new Dictionary<int, Tuple<string, string>>();

            foreach (var item in list)
            {
                if (!dictWithUniqNodes.ContainsKey(item))
                {
                    dictWithUniqNodes.Add(item, ID++);
                    dictWithUniqNodesRef.Add(ID - 1, item);
                }
            }
            nodesCount = dictWithUniqNodes.Count;
            label_status.Content = "STATUS: Pobrano " + nodesCount + " węzłów!";
            Mouse.OverrideCursor = Cursors.Arrow;

            

            int counter2 = 0;

            foreach (var item in dictWithUniqNodes)
            {
                foreach (var item2 in dictWithUniqNodes.Skip(counter2+1))
                {

                    int dist = Dist(Convert.ToDouble(item.Key.Item2), Convert.ToDouble(item.Key.Item1),
                        Convert.ToDouble(item2.Key.Item2), Convert.ToDouble(item2.Key.Item1));

                    if (dist <= 30)
                    {
                        var id = item.Value < item2.Value ? item.Value + "-" + item2.Value : item2.Value + "-" + item.Value;

                        if (!LinkList.ContainsKey(id))
                        {
                            LinkList.Add(id, dist);
                        }                       
                    }
                }
                counter2++;
            }

            linkCount = LinkList.Count;

            if ((linkCount > 0) && (nodesCount > 0))
            {
                button_load.IsEnabled = true;
                button_save.IsEnabled = true;
            }
            else
            {
                button_load.IsEnabled = false;
                button_save.IsEnabled = false;
                label_status.Content = "STATUS: W podanym obszarze, w podanym czasie nic nie znaleziono!";
            }
        }

        private int Dist(double Lat1, double Lon1, double Lat2, double Lon2)
        {
            const double sr = 12756.274;

            double a, b;

            a = (Lon2 - Lon1) * Math.Cos(Lat1 * Math.PI / 180);
            b = (Lat2 - Lat1);

            return (int)(Math.Sqrt(a * a + b * b) * Math.PI * sr / 360);
        }

        private void button_save_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.DefaultExt = ".csv"; // Default file extension
            dlg.FileName = nodesCount + "Q" + linkCount + "KTI-AIS-API"; // Default file name
            dlg.Filter = "CSV|*.csv"; // Filter files by extension
            Nullable<bool> result = dlg.ShowDialog();
            if (result == true)
            {
                SaveToString();
                File.WriteAllText(dlg.FileName, toFileString);
            }
            
            label_status.Content = "STATUS: Zapisano!";
        }

        private void SaveToString()
        {
            var csv = new StringBuilder();

            foreach (var item in LinkList)
            {
                string[] splitID = item.Key.Split('-');

                int id1 = Convert.ToInt32(splitID[0]);
                int id2 = Convert.ToInt32(splitID[1]);

                string lat1 = dictWithUniqNodesRef[id1].Item1.Replace(',', '.');
                string lon1 = dictWithUniqNodesRef[id1].Item2.Replace(',', '.');

                string lat2 = dictWithUniqNodesRef[id2].Item1.Replace(',', '.');
                string lon2 = dictWithUniqNodesRef[id2].Item2.Replace(',', '.');

                var newline = string.Format("{0},{1},{2},{3},{4}", lat1, lon1, lat2, lon2, item.Value);


                csv.AppendLine(newline);
            }
            toFileString = csv.ToString();
        }

        private void button_load_Click(object sender, RoutedEventArgs e)
        {
            if (toFileString == null)
            {
                SaveToString();
            }

            label_status.Content = "STATUS: Wczytano!";
            
        }
    }
}
