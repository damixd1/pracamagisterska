﻿using System;
using System.Collections.Generic;
using Symulator;
using System.Text;
using System.IO;

namespace GraphGenerator
{
    public static class RandomGraphGenerator
    {
        public static List<Router> Generate(int max, int maxLink)
        {
            var linkCount = 0;
            int Lelvels = 0;
            if (max < 100)
            {
                Lelvels = 10;
            }
            else if (max < 1000)
            {
                Lelvels = 30;
            }
            else if (max < 3000)
            {
                Lelvels = 60;
            }
            else
            {
                Lelvels = 100;
            }

            var randomize = new Random(DateTime.Now.Millisecond);
            var nodes = new List<Router>();

            var temp1 = new List<Router>();
            var temp2 = new List<Router>();

            temp1.Add(new Router { RouterID = 1 }); //ROOT

            for (var i = 0; i < Lelvels; i++) //Poziomy
            {
                var MaxNodesinLevel = randomize.Next(1, 1 + i + Lelvels * 2);
                var metric = 2;

                for (var j = 0; j < MaxNodesinLevel; j++) //Ilość węzłów w każdym poziomie
                {
                    if (nodes.Count + temp2.Count + temp1.Count < max)
                    {
                        temp2.Add(new Router { RouterID = (1 + i) * 100 + j + 1 });
                    }
                }

                //łączenie do rodzica
                for (var j = 0; j < MaxNodesinLevel; j++)
                {
                    if (nodes.Count + j + 1 + temp1.Count < max)
                    {
                        if (temp1.Count == 1)
                        {
                            if (randomize.Next(0, 101) <= maxLink)
                            {
                                metric = randomize.Next(1, 10);
                                temp2[j].AddInterface(temp1[0], metric);
                                temp1[0].AddInterface(temp2[j], metric);
                                linkCount++;
                            }
                        }
                        else
                        {
                            foreach (var item in temp1)
                            {
                                if (randomize.Next(0, 101) <= maxLink)
                                {
                                    metric = randomize.Next(1, 10);
                                    temp2[j].AddInterface(item, metric);
                                    item.AddInterface(temp2[j], metric);
                                    linkCount++;
                                }
                            }
                        }
                    }
                }

                //łączanie między węzłami tego samego poziomu

                for (var j = 0; j < MaxNodesinLevel - 1; j++)
                {
                    if (nodes.Count + j + 1 + temp1.Count < max)
                    {
                        if (randomize.Next(0, 101) <= maxLink)
                        { 
                            metric = randomize.Next(1, 10);
                            temp2[j].AddInterface(temp2[j + 1], metric);
                            temp2[j + 1].AddInterface(temp2[j], metric);
                            linkCount++;
                        }
                    }
                }

                foreach (var item in temp1)
                {
                    nodes.Add(item);
                }
                temp1 = temp2;
                temp2 = new List<Router>();
            }

            foreach (var item in temp1)
            {
                nodes.Add(item);
            }

            Stats.Instance.linkCount = linkCount-1;
            Stats.Instance.ShowGraphStats(nodes.Count, linkCount-1);

            var tempDict = new Dictionary<string, int>();

            foreach (var item in nodes)
            {
                foreach (var item2 in item.Interfaces)
                {
                    var value = item.RouterID < item2.Key.RouterID ? item.RouterID + "-" + item2.Key.RouterID : item2.Key.RouterID + "-" + item.RouterID;

                    if(!tempDict.ContainsKey(value))
                    {
                        tempDict.Add(value, item2.Value);
                    }
                }
            }

            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.DefaultExt = ".csv"; // Default file extension
            dlg.FileName = nodes.Count + "Q" + (linkCount - 1); // Default file name
            dlg.Filter = "CSV|*.csv"; // Filter files by extension
            Nullable<bool> result = dlg.ShowDialog();
            if (result == true)
            {
                var csv = new StringBuilder();

                foreach (var item in tempDict)
                {
                    string[] splitID = item.Key.Split('-');

                    string newline;

                    if (Convert.ToInt32(splitID[0]) == 1)
                    {
                        newline = string.Format("{0},{1},{2},{3},{4}", CsvParser.lat2, CsvParser.lon2, splitID[1], splitID[1], item.Value);
                    }
                    else
                    {
                        newline = string.Format("{0},{1},{2},{3},{4}", splitID[0], splitID[0], splitID[1], splitID[1], item.Value);
                    }
                    
                    csv.AppendLine(newline);
                }

                File.WriteAllText(dlg.FileName, csv.ToString());
            }

            return nodes;
        }
    }
}