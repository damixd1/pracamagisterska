﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Symulator;

namespace GraphGenerator
{
    public static class CsvParser
    {
        public static double lat2 { get; set; } = 54.3520500;
        public static double lon2 { get; set; } = 18.6463700;

        private static double DistanceB2P(double lat1, double lon1)
        {
            var temp1 = Math.Pow(lat2 - lat1, 2);
            var temp2 = Math.Pow(lon2 - lon1, 2);

            return Math.Sqrt(temp1 + temp2);
        }

        public static List<Router> Parse(Stream stream)
        {
            var nodes = new Dictionary<string, Router>();
            var listOfPreLinks = new List<PreLink>();
            var preNodes = new Dictionary<Tuple<double, double>, double>();

            var reader = new StreamReader(stream);

            reader.BaseStream.Position = 0;

            while (reader.Peek() != -1)
            {


                var line = reader.ReadLine();
                var data = line.Split(',');
                var pattern = "\\.+";
                var replacement = ",";
                var rgx = new Regex(pattern);

                for (var i = 0; i <= 4; i++)
                {
                    data[i] = rgx.Replace(data[i], replacement);
                }


                listOfPreLinks.Add(new PreLink
                {
                    lat1 = Convert.ToDouble(data[0]),
                    lon1 = Convert.ToDouble(data[1]),
                    lat2 = Convert.ToDouble(data[2]),
                    lon2 = Convert.ToDouble(data[3]),
                    distance = Convert.ToInt32(data[4])
                });
            }

            foreach (var item in listOfPreLinks)
            {
                var temp1 = new Tuple<double, double>(item.lat1, item.lon1);
                var temp2 = new Tuple<double, double>(item.lat2, item.lon2);

                if (!preNodes.ContainsKey(temp1))
                {
                    preNodes.Add(temp1, DistanceB2P(temp1.Item1, temp1.Item1));
                }

                if (!preNodes.ContainsKey(temp2))
                {
                    preNodes.Add(temp2, DistanceB2P(temp2.Item1, temp2.Item1));
                }
            }

            var temp_min = preNodes.Aggregate((v1, v2) => v1.Value < v2.Value ? v1 : v2).Key;

            nodes.Add(temp_min.Item1 + "+" + temp_min.Item2, new Router { RouterID = 1 });
            var indexID = 2;
            foreach (var item in preNodes)
            {
                if (!item.Key.Equals(temp_min))
                {
                    nodes.Add(item.Key.Item1 + "+" + item.Key.Item2, new Router { RouterID = indexID++ });
                }
            }

            var count = 0;
            foreach (var item in listOfPreLinks)
            {
                var tempLat1Lon1 = item.lat1 + "+" + item.lon1;
                var tempLat2Lon2 = item.lat2 + "+" + item.lon2;

                var tempRouter1 = nodes[tempLat1Lon1];
                var tempRouter2 = nodes[tempLat2Lon2];

                if (item.distance <= 30)
                {
                    count++;
                    var metric = CalculateLQI(item.distance);
                    tempRouter1.AddInterface(tempRouter2, metric);
                    tempRouter2.AddInterface(tempRouter1, metric);
                }
            }
            Stats.Instance.linkCount = count;
            Stats.Instance.ShowGraphStats(nodes.Values.ToList().Count, count);
            return nodes.Values.ToList();
        }

        private static int CalculateLQI(int distance)
        {
            var randomize = new Random(DateTime.Now.Millisecond);

            var LQI_Dict = new Dictionary<string, int>();


            float A = 3/6;
            float B = 2/6;
            float C = 1/6;

            var wifi = 0;
            var _3g = 0;
            var wcdma = 0;
            var radwin = 0;
            var wimax = 0;
            var lte = 0;

            var LQI_Speed = 0;
            var LQI_Random = 0;
            var LQI_Distance = 0;
            var LQI = 0;

            if (distance <= 4)
            {
                LQI_Speed = 150 - 75 * distance / 2;
                LQI_Distance = (int)(1500 * GaussDistribution(0, 4, distance));
                LQI_Random = randomize.Next(-151, 151);

                wifi = (int)(A * LQI_Speed + B * LQI_Distance + C * LQI_Random);
                LQI_Dict.Add("wifi", wifi);
            }

            if (distance <= 12)
            {
                LQI_Speed = (int)(19.75 - 1.64583 * distance);
                LQI_Distance = (int)(1500 * GaussDistribution(0, 12, distance));
                LQI_Random = randomize.Next(-151, 151);

                _3g = (int)(A * LQI_Speed + B * LQI_Distance + C * LQI_Random);
                LQI_Dict.Add("3g", _3g);
            }

            if (distance <= 15)
            {
                LQI_Speed = (int)(13.6 - 0.906667 * distance);
                LQI_Distance = (int)(1500 * GaussDistribution(0, 15, distance));
                LQI_Random = randomize.Next(-151, 151);

                wcdma = (int)(A * LQI_Speed + B * LQI_Distance + C * LQI_Random);
                LQI_Dict.Add("wcdma", wcdma);
            }

            if (distance <= 20)
            {
                LQI_Speed = 50 - 5 * distance / 2;
                LQI_Distance = (int)(1500 * GaussDistribution(0, 20, distance));
                LQI_Random = randomize.Next(-151, 151);

                radwin = (int)(A * LQI_Speed + B * LQI_Distance + C * LQI_Random);
                LQI_Dict.Add("radwin", radwin);
            }

            if (distance <= 25)
            {
                LQI_Speed = 90 - 18 * distance / 5;
                LQI_Distance = (int)(1500 * GaussDistribution(0, 25, distance));
                LQI_Random = randomize.Next(-151, 151);

                wimax = (int)(A * LQI_Speed + B * LQI_Distance + C * LQI_Random);
                LQI_Dict.Add("wimax", wimax);
            }

            if (distance <= 30)
            {
                LQI_Speed = 100 - 100 * distance / 30;
                LQI_Distance = (int)(1500 * GaussDistribution(0, 30, distance));
                LQI_Random = randomize.Next(-151, 151);

                lte = (int)(A * LQI_Speed + B * LQI_Distance + C * LQI_Random);
                LQI_Dict.Add("lte", lte);
            }

            LQI = LQI_Dict.Aggregate((v1, v2) => v1.Value > v2.Value ? v1 : v2).Value;

            return LQI;
        }

        public static double GaussDistribution(int expectedValue, int deviation, int x)
        {
            var temp1 = 1 / (deviation * Math.Sqrt(2 * Math.PI));
            var temp2 = -Math.Pow(x - expectedValue, 2) / (2 * Math.Pow(deviation, 2));

            return temp1 * Math.Pow(Math.E, temp2);
        }

        private class PreLink
        {
            public double lon1 { get; set; }
            public double lat1 { get; set; }
            public double lon2 { get; set; }
            public double lat2 { get; set; }
            public int distance { get; set; }
        }
    }
}