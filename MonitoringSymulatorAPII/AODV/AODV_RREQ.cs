﻿using System;
using Frames;

namespace AODV
{
    public class AODV_RREQ : Frame
    {
        public AODV_RREQ()
        {
            lengthOfHeader = 59;
        }

        public int HopC { get; set; }
        public int Metric { get; set; }
        public int RREQ_ID { get; set; }
        public string DestinationAddress { get; set; }
        public string OriginatorAddress { get; set; }
        public int DestinationSequenceNumber { get; set; }
        public int OriginatorSequenceNumber { get; set; }
        public int TTL { get; set; }
        /*public int ElementID { get; set; }
        public int Length { get; set; }
        public int Flags { get; set; }
        */
    }
}