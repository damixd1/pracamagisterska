﻿using System;
using Frames;

namespace AODV
{
    public class AODV_RREP : Frame
    {
        public AODV_RREP()
        {
            lengthOfHeader = 53;
        }

        public int HopC { get; set; }
        public int Metric { get; set; }
        public string DestinationAddress { get; set; }
        public string OriginatorAddress { get; set; }
        public int DestinationSequenceNumber { get; set; }
        public int LifeTime { get; set; } //in milisecond
        /*
        public int ElementID { get; set; }
        public int Length { get; set; }
        public int TTL { get; set; }
        public int Flags { get; set; }
        */


    }
}