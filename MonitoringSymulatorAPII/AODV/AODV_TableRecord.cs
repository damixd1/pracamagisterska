﻿using System;
using Symulator;

namespace AODV
{

    public class AODV_TableRecord
    {
        public string DestinationIPv6 { get; set; }
        public int DestinationSequenceNumber { get; set; }
        public Router Interface { get; set; }
        public int HopCount { get; set; }
        public int Metric { get; set; }
        public DateTime LifeTime { get; set; }
    }
}