﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using GraphGenerator;
using System.IO;
using System.Text;
using Symulator;

namespace Symulator
{

    public enum MonitoringType
    {
        Forced,
        Unforced,
        SNMP
    }

    public partial class Program : Window
    {
        public Stream stream;
        public static int timer = 3000;
        public Thread thread;
        private static bool _endOfMonitoring = false;
        public static bool accident { get; set; } = false;
        private string filename = " ";
        private List<Router> listOfRouters = null;
        public static Stream tempStream { get; set; } = null;

        public Program()
        {
            InitializeComponent();
            textBox_NodesCount.Visibility = Visibility.Hidden;
            label_NodesCount.Visibility = Visibility.Hidden;
            comboBox_MonitoringType.Items.Add("EHBMPvF");
            comboBox_MonitoringType.Items.Add("EHBMPvU");
            comboBox_MonitoringType.Items.Add("SNMP");

            comboBox_MonitoringType.SelectedIndex = 0;


            comboBox_MonitoringDataSize.Items.Add("64");
            comboBox_MonitoringDataSize.Items.Add("128");
            comboBox_MonitoringDataSize.Items.Add("256");
            comboBox_MonitoringDataSize.Items.Add("512");

            comboBox_MonitoringDataSize.SelectedIndex = 1;

            comboBox_GenerateOrLoadGraph.Items.Add("Wczytaj");
            comboBox_GenerateOrLoadGraph.Items.Add("Wygeneruj");
            comboBox_GenerateOrLoadGraph.Items.Add("KTI AIS API");

            comboBox_GenerateOrLoadGraph.SelectedIndex = 0;

            comboBox_StatsModuleInterval.Items.Add("1000");
            comboBox_StatsModuleInterval.Items.Add("3000");
            comboBox_StatsModuleInterval.Items.Add("10000");
            comboBox_StatsModuleInterval.Items.Add("Podaj własny:");
            comboBox_StatsModuleInterval.SelectedIndex = 0;

            textBox_PayloadSize.Text = "1440";
            textBox_NodesCount.Text = "100";

            comboBox_ChooseRootPosition.Items.Add("Centrum Gdańska");
            comboBox_ChooseRootPosition.Items.Add("Inne - Podaj:");
            comboBox_ChooseRootPosition.SelectedIndex = 0;

            label_Lat.Visibility = Visibility.Hidden;
            label_Lon.Visibility = Visibility.Hidden;
            textBox_Lat.Visibility = Visibility.Hidden;
            textBox_Lon.Visibility = Visibility.Hidden;

            textBox_Lat.Text = "54,35205";
            textBox_Lon.Text = "18,64637";

            slider_LinksCountProb.Visibility = Visibility.Hidden;


            textBox_intervalSym.Text = "1000";

            textBox_statsInterval.Text = "1000";

            Stats.Instance.instance2 = this;

            int workerThreads; int completionPortThreads;
            ThreadPool.GetMaxThreads(out workerThreads, out completionPortThreads);

        }

        private static void StartStatsModule(MonitoringType type, bool isAccidentPossible)
        {
            accident = false;
            var endOfMonitoring = false;
            while (!endOfMonitoring)
            {
                //accident = false;
                endOfMonitoring = Stats.Instance.ShowStats(type);
                Thread.Sleep(timer);
                if ((Stats.Instance.TBRPtableCount != 0) && (isAccidentPossible == true))
                {
                    accident = true;
                }
                
            }
        }

        private void Run(int monitoringType, int monitoringDataSize, int generateOrLoadGraph, int sizeGraphToGenerate, int statsInterval, int payLoad, bool accidentPosible, int maxLink, int symUinterval, int moduleInterval)
        {

            Router.maxAddedBytesPerNode = (int)monitoringDataSize;
            Router.maxNodeQuantityToVisit = (payLoad - 40 - 8 - 24) / ((int)monitoringDataSize + 16 + 8);
            Router.payLoadWifi = payLoad;
            Router.timeIntervalUms = symUinterval;
            Router.isaccidentposible = accidentPosible;

            var randomize = new Random(DateTime.Now.Millisecond);

            var nodes = new List<Router>();

            

            if (generateOrLoadGraph != 1)
            {
                nodes = null;
                nodes = CsvParser.Parse(stream);
            }
            else
            {
                nodes = RandomGraphGenerator.Generate(sizeGraphToGenerate, maxLink);
            }


            switch (statsInterval)
            {
                case 0:
                    timer = 1*1000;
                    break;
                case 1:
                    timer = 3*1000;
                    break;
                case 2:
                    timer = 10*1000;
                    break;
                case 3:
                    timer = moduleInterval;
                    break;
            }

            Stats.Instance.Showinterval(timer);

            nodes[0].MAC = AdressGenerator.GenerateNewMac(randomize);
            nodes[0].IPv6 = AdressGenerator.GenerateNewIPv6(randomize);

            foreach (var item in nodes.Skip(1))
            {
                item.MAC = AdressGenerator.GenerateNewMac(randomize);
                item.IPv6 = AdressGenerator.GenerateNewIPv6(randomize);
                Stats.Instance.nodesToMonitor.GetOrAdd(item.IPv6, false);
            }


            listOfRouters = nodes;
            foreach (var item in nodes)
            {
                Stats.Instance.refIPtoID.Add(item.IPv6,item.RouterID);
            }


                switch ((int) monitoringType)
            {
                case 0:
                    foreach (Router item in nodes)
                    {
                        item.Start(nodes, MonitoringType.Forced);
                    }
                    StartStatsModule(MonitoringType.Forced, accidentPosible);
                    break;
                case 1:
                    foreach (Router item in nodes)
                    {
                        item.Start(nodes, MonitoringType.Unforced);
                    }
                    StartStatsModule(MonitoringType.Unforced, accidentPosible);
                    break;
                case 2:
                    foreach (Router item in nodes)
                    {
                        item.Start(nodes, MonitoringType.SNMP);
                    }
                    StartStatsModule(MonitoringType.SNMP, accidentPosible);
                    break;
                default:
                    //Console.WriteLine("Zły wybór!");
                    break;
            }
        }

        private void button_StartSymulation_Click(object sender, RoutedEventArgs e)
        {

            if (thread != null)
            {
                thread.Abort();
            }           

            int maxAddedBytesPerNode = (int)Math.Pow(2, (6 + comboBox_MonitoringDataSize.SelectedIndex));
            int choise = comboBox_MonitoringType.SelectedIndex;
            int generchoise = comboBox_GenerateOrLoadGraph.SelectedIndex;
            int gener = 0;

            if (generchoise == 1)
            {
                int.TryParse(textBox_NodesCount.Text, out gener);
                if (gener == 0)
                {
                    MessageBox.Show("Zła wartośc pola: Ilość węzłów", "Błąd", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
            }
            else
            {
                if (stream == null)
                {
                    MessageBox.Show("Nie wczytano pliku CSV", "Błąd", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
            }

            bool accidentPosible = false;

            if (checkBox_ActiveLinksBrake.IsChecked == true)
            {
                accidentPosible = true;
            }
            else
            {
                accidentPosible = false;
            }


            int poleDanych = 0;

            int.TryParse(textBox_PayloadSize.Text, out poleDanych);

            if (poleDanych == 0)
            {
                MessageBox.Show("Zła wartośc pola: Rozmiaru pola danych protokołu łącza danych", "Błąd", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            if (comboBox_ChooseRootPosition.SelectedIndex == 0)
            {
                CsvParser.lat2 = 54.3520500;
                CsvParser.lon2 = 18.6463700;
            }
            else
            {
                double lat_temp = 0;
                double lon_temp = 0;
                
                double.TryParse(textBox_Lat.Text.Replace('.',','), out lat_temp);
                if (lat_temp == 0)
                {
                    MessageBox.Show("Zła wartośc pola: Szerokość geograficzna", "Błąd", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                double.TryParse(textBox_Lon.Text.Replace('.', ','), out lon_temp);
                if (lon_temp == 0)
                {
                    MessageBox.Show("Zła wartośc pola: Długość geograficzna", "Błąd", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                CsvParser.lat2 = lat_temp;
                CsvParser.lon2 = lon_temp;
            }

            int linkMax = (int)slider_LinksCountProb.Value;


            int statschoise = comboBox_StatsModuleInterval.SelectedIndex;

            int poleInterwalSymU = 0;
            if (comboBox_MonitoringType.SelectedIndex == 1)
            {
                
                int.TryParse(textBox_intervalSym.Text, out poleInterwalSymU);

                if (poleInterwalSymU <= 0)
                {
                    MessageBox.Show("Zła wartośc pola: Okres występowania pakietów tła", "Błąd", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
            }

            int intervalSymModule = 0;

            if (comboBox_StatsModuleInterval.SelectedIndex == 3)
            {

                int.TryParse(textBox_statsInterval.Text, out intervalSymModule);

                if (intervalSymModule <= 0)
                {
                    MessageBox.Show("Zła wartośc pola: Własny interwał statystyk", "Błąd", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
            }



            button_StartSymulation.IsEnabled = false;
            thread = new Thread(() => Run(choise, maxAddedBytesPerNode, generchoise, gener, statschoise, poleDanych, accidentPosible, linkMax, poleInterwalSymU, intervalSymModule))
            {
                IsBackground = true
            };
            thread.Start();

        }

        private void comboBox_GraphLoadOrGenerate_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (comboBox_GenerateOrLoadGraph.SelectedIndex == 1)
            {
                button_LoadGraph.Visibility = Visibility.Hidden;
                textBox_NodesCount.Visibility = Visibility.Visible;
                label_NodesCount.Visibility = Visibility.Visible;
                slider_LinksCountProb.Visibility = Visibility.Visible;
                label_NameOfLoadedGraph.Content = ((int)slider_LinksCountProb.Value).ToString();
                label_LinksCount.Visibility = Visibility.Visible;
            }
            else
            {
                button_LoadGraph.Visibility = Visibility.Visible;
                textBox_NodesCount.Visibility = Visibility.Hidden;
                label_NodesCount.Visibility = Visibility.Hidden;
                label_NameOfLoadedGraph.Visibility = Visibility.Visible;
                slider_LinksCountProb.Visibility = Visibility.Hidden;
                label_NameOfLoadedGraph.Content = filename;
                label_LinksCount.Visibility = Visibility.Hidden;
            }
            
        }

        private void button_LoadGraph_Click(object sender, RoutedEventArgs e)
        {

            if (comboBox_GenerateOrLoadGraph.SelectedIndex == 0)
            {
                Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
                // Set filter for file extension and default file extension 
                dlg.DefaultExt = ".csv";
                dlg.Filter = "CSV files (*.csv) | *.csv";

                // Display OpenFileDialog by calling ShowDialog method 
                Nullable<bool> result = dlg.ShowDialog();
                if (!dlg.FileName.Equals(""))
                {
                    string ext = System.IO.Path.GetExtension(dlg.FileName);
                    if (ext.Equals(".csv"))
                    {
                        if (stream != null)
                        {
                            stream.Close();
                        }
                        stream = new FileStream(dlg.FileName, FileMode.Open, FileAccess.Read, FileShare.Read);

                        string[] temp = dlg.FileName.ToString().Split('\u005c');
                        string[] temp2 = temp.Last().Split('_');

                        if (temp2.Length < 2)
                        {
                            label_NameOfLoadedGraph.Content = "Wczytano: " + temp.Last();
                            filename = temp.Last();
                        }
                        else
                        {
                            label_NameOfLoadedGraph.Content = "Wczytano: " + temp2[0] + "__" + temp2[1];
                            filename = temp2[0] + "__" + temp2[1];
                        }

                    }
                }
            }
            else
            {
               API api = new API();
               api.ShowDialog();

                if (api.toFileString != null)
                {
                    stream = GenerateStreamFromString(api.toFileString);
                    label_NameOfLoadedGraph.Content = "Wczytano z KTI AIS API";
                }              
            }
        }

        private Stream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            if (stream != null)
            {
                stream.Close();
            }        
        }

        private void comboBox_ChooseRootPosition_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (comboBox_ChooseRootPosition.SelectedIndex == 0)
            {
                label_Lat.Visibility = Visibility.Hidden;
                label_Lon.Visibility = Visibility.Hidden;
                textBox_Lat.Visibility = Visibility.Hidden;
                textBox_Lon.Visibility = Visibility.Hidden;

            }
            else
            {
                label_Lat.Visibility = Visibility.Visible;
                label_Lon.Visibility = Visibility.Visible;
                textBox_Lat.Visibility = Visibility.Visible;
                textBox_Lon.Visibility = Visibility.Visible;
            }
        }

        private void slider_LinksCountProb_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            label_NameOfLoadedGraph.Content = ((int)slider_LinksCountProb.Value).ToString();
        }

        private void comboBox_MonitoringType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (comboBox_MonitoringType.SelectedIndex == 1)
            {
                label_IntervalSymU.Visibility = Visibility.Visible;
                textBox_intervalSym.Visibility = Visibility.Visible;
            }
            else
            {
                label_IntervalSymU.Visibility = Visibility.Hidden;
                textBox_intervalSym.Visibility = Visibility.Hidden;
            }
        }

        private void comboBox_StatsModuleInterval_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (comboBox_StatsModuleInterval.SelectedIndex == 3)
            {
                textBox_statsInterval.Visibility = Visibility.Visible;
            }
            else
            {
                textBox_statsInterval.Visibility = Visibility.Hidden;
            }
        }

        private void CloseApp(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void SaveRaport(object sender, RoutedEventArgs e)
        {
            if (Router.isMonitored == true && comboBox_MonitoringType.SelectedIndex == 0)
            {
                Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
                dlg.DefaultExt = ".csv"; // Default file extension
                dlg.FileName = "Raport - odwiedzane węzły przez pakiety w EHBMPvF"; // Default file name
                dlg.Filter = "CSV|*.csv"; // Filter files by extension
                Nullable<bool> result = dlg.ShowDialog();
                if (result == true)
                {
                    var csv = new StringBuilder();
                    string newline = null;
                    string newline2 = null;
                    int packetCount = Stats.Instance.raportList.Count;

                    for (int i = 0; i < packetCount; i++)
                    {
                        newline += "Pakiet nr " + (i + 1) + ";";
                        newline += ";";
                        newline2 += "ID wezlow:;;";
                    }
                    csv.AppendLine(newline);
                    csv.AppendLine(newline2);
                    newline = null;

                    int maxSize = 0;

                    foreach (var item in Stats.Instance.raportList)
                    {
                        if (maxSize < item.Count)
                        {
                            maxSize = item.Count;
                        }
                    }

                    for (int i = 0; i < maxSize; i++)
                    {
                        for (int j = 0; j < packetCount; j++)
                        {

                            if (Stats.Instance.raportList[j].Count < i + 1)
                            {
                                newline += ";";
                            }
                            else
                            {
                                newline += Stats.Instance.raportList[j][i] + ";";
                            }
                            newline += ";";

                        }

                        csv.AppendLine(newline);
                        newline = null;
                    }


                    File.WriteAllText(dlg.FileName, csv.ToString());
                }
            }
            else
            {
                MessageBox.Show("Raport dostępny tylko dla EHBMPvF po zakończeniu symulacji", "Błąd", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void checkBox_showMonitoredNodes_Click(object sender, RoutedEventArgs e)
        {
            if (checkBox_showMonitoredNodes.IsChecked == true)
            {
                Stats.Instance.adInfoAboutMonioredNodes = true;
            }
            else
            {
                Stats.Instance.adInfoAboutMonioredNodes = false;
            }
        }

        private void SaveGraphLinks(object sender, RoutedEventArgs e)
        {
            if (listOfRouters != null && Router.isMonitored == true)
            {
                Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
                dlg.DefaultExt = ".csv"; // Default file extension
                dlg.FileName = "Połączenia między węzłami"; // Default file name
                dlg.Filter = "CSV|*.csv"; // Filter files by extension
                Nullable<bool> result = dlg.ShowDialog();
                if (result == true)
                {
                    var csv = new StringBuilder();
                    string newline = ";";

                    int size = listOfRouters.Count;

                    int[,] tabLinks = new int[size, size];

                    csv.AppendLine("0 - brak polaczenia;");
                    csv.AppendLine("1 - polaczenie;");
                    for (int i = 0; i < size; i++)
                    {
                        for (int j = 0; j < size; j++)
                        {
                            tabLinks[i, j] = 0;
                        }

                        newline += "W" + (i + 1) + ";";
                    }
                    csv.AppendLine(newline);

                    foreach (var item in listOfRouters)
                    {
                        foreach (var itemInterfaces in item.Interfaces)
                        {
                            tabLinks[item.RouterID - 1, itemInterfaces.Key.RouterID - 1] = 1;
                            tabLinks[itemInterfaces.Key.RouterID - 1, item.RouterID - 1] = 1;
                        }
                    }
                    

                    for (int i = 0; i < size; i++)
                    {
                        newline = "W" + (i + 1) + ";";
                        for (int j = 0; j < size; j++)
                        {
                            if (i == j)
                            {
                                newline += "x;";
                            }
                            else
                            {
                                newline += tabLinks[i, j] + ";";
                            }
                            
                        }

                        
                        csv.AppendLine(newline);
                    }
                    File.WriteAllText(dlg.FileName, csv.ToString());
                }
            }
            else
            {
                MessageBox.Show("Informacje o połączeniach w grafie dostępne po zakończeniu symulacji", "Błąd", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
